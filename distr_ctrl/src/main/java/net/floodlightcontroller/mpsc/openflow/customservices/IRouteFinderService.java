package net.floodlightcontroller.mpsc.openflow.customservices;

import net.floodlightcontroller.core.module.IFloodlightService;
import net.floodlightcontroller.mpsc.discovery.model.INode;
import net.floodlightcontroller.mpsc.discovery.model.ITopology;

import java.util.List;
import java.util.Map;

/**
 * Created by chemo_000 on 2/10/2015.
 */
public interface IRouteFinderService extends IFloodlightService //todo java docs
{
    public enum Type{NM, NMGEN, ED, IBF, EBFS}

    public <T> Map<List<INode<T>>, Double> findRoutesAndBw(T srcID, T dstID, boolean isSplittable, double bw);

    public <T> Map<List<INode<T>>, Double> findRoutesAndBwCost(T srcID, T dstID, boolean isSplittable, double bw, double cost);

    public <T> void allocateRoute(List<INode<T>> path, double bw);

    public <T> void allocateRoute(List<INode<T>> path, String srcIP, String dstIP, double bw);

    public <T> void removeRoute(List<INode<T>> path, String srcIP, String dstIP, double bw);

    public ITopology getTopo();
}
