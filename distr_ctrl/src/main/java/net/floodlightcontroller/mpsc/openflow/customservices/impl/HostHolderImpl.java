package net.floodlightcontroller.mpsc.openflow.customservices.impl;

import net.floodlightcontroller.mpsc.openflow.customservices.IHostHolderService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.projectfloodlight.openflow.types.DatapathId;
import org.projectfloodlight.openflow.types.IPv4Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by chemo_000 on 2/15/2015.
 */
public class HostHolderImpl implements IHostHolderService {
    private static Logger log = LoggerFactory.getLogger(HostHolderImpl.class);

    private Map<Pair<IPv4Address, DatapathId>, Short> hostsAndSwitchesToPorts;

    private Map<IPv4Address, Double> hostsAllocatedResources;

    private static HostHolderImpl instance = new HostHolderImpl();

    public static HostHolderImpl getInstance() {
        return instance;
    }

    private HostHolderImpl() {
        hostsAndSwitchesToPorts = new ConcurrentHashMap<>();
        hostsAllocatedResources = new ConcurrentHashMap<>();
    }


    @Override
    public synchronized boolean containsIPAndDPID(IPv4Address ip, DatapathId dpid)//todo add due dates for entries
    {
        Pair<IPv4Address, DatapathId> k = new ImmutablePair<>(ip, dpid);
        return hostsAndSwitchesToPorts.containsKey(k);
    }

    @Override
    public synchronized void put(Pair<IPv4Address, DatapathId> k, Short v) {
        if (hostsAndSwitchesToPorts.containsKey(k))
            hostsAndSwitchesToPorts.remove(k);

        hostsAndSwitchesToPorts.put(k, v);

        //init host allocated resources
        if (!hostsAllocatedResources.containsKey(k.getKey()))
            hostsAllocatedResources.put(k.getKey(), 0.0);

        if (log.isTraceEnabled())
            log.trace("[NM-OFService]Next IP=" + k
                    + " was added. All known hosts are: " + hostsAndSwitchesToPorts.keySet());
    }

    @Override
    public Set<Pair<DatapathId, Short>> getSwitchesAndPortsByHost(IPv4Address hostIP) {
        Set<Pair<DatapathId, Short>> switchesAndPorts = new HashSet<>();

        for (Map.Entry<Pair<IPv4Address, DatapathId>, Short> hostToSwithAndPort : hostsAndSwitchesToPorts.entrySet())
            if (hostToSwithAndPort.getKey().getLeft().equals(hostIP)) {
                DatapathId k = hostToSwithAndPort.getKey().getRight();
                Short v = hostToSwithAndPort.getValue();

                switchesAndPorts.add(new ImmutablePair<>(k, v));
            }

        return switchesAndPorts;
    }

    @Override
    public Set<Pair<IPv4Address, Short>> getAllHostsAndPortsBySwitch(DatapathId dpid) {
        Set<Pair<IPv4Address, Short>> hostsAndPorts = new HashSet<>();

        for (Map.Entry<Pair<IPv4Address, DatapathId>, Short> hostToSwithAndPort : hostsAndSwitchesToPorts.entrySet())
            if (hostToSwithAndPort.getKey().getRight().equals(dpid)) {
                IPv4Address k = hostToSwithAndPort.getKey().getLeft();
                Short v = hostToSwithAndPort.getValue();

                hostsAndPorts.add(new ImmutablePair<>(k, v));
            }

        return hostsAndPorts;
    }

    @Override
    public Map<Pair<IPv4Address, DatapathId>, Short> getHostsAndSwitchesToPortsMap() {
        return hostsAndSwitchesToPorts;
    }

    @Override
    public Double getHostAllocatedResources(Object k) {
        if (k instanceof IPv4Address && hostsAllocatedResources.containsKey(k))
            return this.hostsAllocatedResources.get(k);
        else
            return null;
    }

    @Override
    public void setHostAllocatedResources(Object k, Double d) {
        if (k instanceof IPv4Address && hostsAllocatedResources.containsKey(k)) {
            IPv4Address key = (IPv4Address) k;
            this.hostsAllocatedResources.remove(key);
            this.hostsAllocatedResources.put(key, d);
        }
    }

    @Override
    public void releaseAllHostsAllocatedResources() {
        Set<IPv4Address> keys = new HashSet<>(this.hostsAllocatedResources.keySet());
        this.hostsAllocatedResources.clear();
        for (IPv4Address k : keys)
            this.hostsAllocatedResources.put(k, 0.0);
    }
}
