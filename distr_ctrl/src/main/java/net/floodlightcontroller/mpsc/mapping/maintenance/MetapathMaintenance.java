package net.floodlightcontroller.mpsc.mapping.maintenance;

import net.floodlightcontroller.mpsc.discovery.model.ILink;
import net.floodlightcontroller.mpsc.discovery.model.INode;
import net.floodlightcontroller.mpsc.discovery.model.ITopology;
import net.floodlightcontroller.mpsc.mapping.routing.GeneralKBestNM;
import net.floodlightcontroller.mpsc.web.model.MPath;
import net.floodlightcontroller.mpsc.web.model.VirtualLink;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;

/**
 * Created by dmitriichemodanov on 10/10/18.
 */
public class MetapathMaintenance {
    private GeneralKBestNM kBestNM;
    private MetapathCostComparator metapathCostComparator = new MetapathCostComparator();
    private int iter = 0;
    private int ctrlMessages = 0;
    private Map<String, INode> best_vNodeToPNode;
    private Map<VirtualLink, List<INode>> best_vLinkToPath;
    private double bestObj = Double.POSITIVE_INFINITY;

    /**
     * provides a comparator to sort virtual to physical node mappings by their cost for objective
     */
    public static NodeCostComparator CostComparator = new NodeCostComparator();

    /**
     * Restores failed pieces of SC
     *
     * @param t            - graph
     * @param maxMetapaths - K metapaths policy
     * @param maxIter      - max number of iterations before stop
     * @param isDynamic    - dynamic vs pre-compute policy
     * @return statistic such obj_value(1) and # of ctrl messages (2)
     */
    public Pair<Map<String, INode>, Map<VirtualLink, List<INode>>> allocateSC(List<VirtualLink> vls,
                                                                                        ITopology t, int maxMetapaths,
                                                                                        int maxIter, boolean isDynamic,
                                                                                        Map<String, INode> vNodeToPNode,
                                                                                        Map<VirtualLink, List<INode>> vLinkToPath) {
        int convTime = 0;
        iter = 0;
        //ask all controlles what failed services their pnodes can place [if needed]
//        ctrlMessages = vNodeToPNode.size() == vnr.getVNodeSize() ?
//                0 : 2 * t.getGraph().getNumNodes();

        //clean refs
        bestObj = Double.POSITIVE_INFINITY;
        best_vNodeToPNode = null;
        best_vLinkToPath = null;

        Map<String, INode> vNodeToPNodeInt = new LinkedHashMap<>(vNodeToPNode);
        Map<VirtualLink, List<INode>> vLinkToPathInt = new LinkedHashMap<>(vLinkToPath);

        try {
            metapathFlib(getVRoot(vls), vls, t, null, vNodeToPNodeInt, vLinkToPathInt, vNodeToPNode.keySet(),
                    vLinkToPath.keySet(), new HashSet<>(), maxMetapaths, maxIter, isDynamic);
        } catch (Exception ex) {
        }

        //release all new resources been used for failed
        vLinkToPathInt.keySet().removeAll(vLinkToPath.keySet());
        vNodeToPNodeInt.keySet().removeAll(vNodeToPNode.keySet());
        releaseAllVNRResourcesBeenUsed(vNodeToPNodeInt, vLinkToPathInt);

        convTime += iter;

        if (best_vLinkToPath != null && best_vNodeToPNode != null) {
            //allocate all new resources
            best_vLinkToPath.keySet().removeAll(vLinkToPath.keySet());
            best_vNodeToPNode.keySet().removeAll(vNodeToPNode.keySet());
            allocateVNR(vls, best_vNodeToPNode, best_vLinkToPath);
            vNodeToPNode.putAll(best_vNodeToPNode);
            //track new mappings
            vLinkToPath.putAll(best_vLinkToPath);
            //List<Double> res = new ArrayList<>(2);
            //res.add(calculateVNRObjective(best_vNodeToPNode, best_vLinkToPath)); //obj value
            //res.add((double) ctrlMessages);
            clearMetaedges(t);
            return new ImmutablePair<>(best_vNodeToPNode, best_vLinkToPath);
        } else
            throw new RuntimeException("[MetapathVNE] Mapping of the VNR has failed after " + convTime + " iterations with the metapaths limit of " + maxMetapaths);
    }

    private void metapathFlib(String nextVN, List<VirtualLink> vls, ITopology t,
                                  Map<String, List<VirtualLink>> vnNhs,
                                  Map<String, INode> vNodeToPNode,
                                  Map<VirtualLink, List<INode>> vLinkToPath,
                                  Set<String> survivedVNs,
                                  Set<VirtualLink> survivedVEs,
                                  Set<String> checkedNodes,
                                  int maxMetapaths, int maxIter, boolean isDynamic) { //todo:consider moving policies to global vars
        ++iter;
        if (iter <= maxIter) {
            //find first from the root non-survived link
            //todo: fix termination + depth tracking -> doesn't work
            Set<String> checkedNodesInt = new HashSet<>();
            boolean isSurvived = true;
            int iterInt = 0;
            while (isSurvived && checkedNodes.size() <= vls.size()) {
                for (VirtualLink vl : vnNhs.get(nextVN))
                    if (!vLinkToPath.containsKey(vl)) {
                        isSurvived = false;
                        checkedNodes.add(nextVN);
                        checkedNodesInt.add(nextVN);
                        break;
                    } else if (!checkedNodes.contains(nextVN)) {
                        checkedNodes.add(nextVN);
                        checkedNodesInt.add(nextVN);
                        nextVN = vl.getSrc().equals(nextVN) ? vl.getDst() : vl.getSrc();
                    } else {
                        checkedNodes.add(nextVN);
                        checkedNodesInt.add(nextVN);
                    }
                if (iterInt++ > 10)
                    if (iterInt % 10 == 0)
                        System.out.print("." + isSurvived);
                ctrlMessages++; //send 'check' request to the next VN's controller
                //if (checkedNodes.size() == vnr.getVNodeSize() - 1)
                //    System.out.print(".");
            }

            List<Pair<VirtualLink, List<INode>>> possibleMetapaths = genMetapath(nextVN, getVirtualNeighbors(vls), t, vNodeToPNode, vLinkToPath,
                    maxMetapaths, isDynamic, vLinkToPath.size());
            for (Pair<VirtualLink, List<INode>> metaPath : possibleMetapaths) {
                //TODO: continue with metapath allocation
                allocateMetapath(metaPath, vNodeToPNode, vLinkToPath);
                double objCurr = calculateVNRObjective(vNodeToPNode, vLinkToPath);
                if (objCurr < bestObj) {
                    ctrlMessages++;
                    metapathFlib(metaPath.getKey().getSrc().equals(nextVN) ?
                                    metaPath.getKey().getDst() : metaPath.getKey().getSrc(),
                            vls, t, vnNhs, vNodeToPNode, vLinkToPath, survivedVNs, survivedVEs, checkedNodes,
                            maxMetapaths, maxIter, isDynamic);

                    if (correctMapping(vls, vNodeToPNode, vLinkToPath)) {
                        double obj = calculateVNRObjective(vNodeToPNode, vLinkToPath);
                        //track best VNE found so far!
                        if (obj < bestObj) {
                            bestObj = obj;
                            best_vLinkToPath = new LinkedHashMap<>(vLinkToPath);
                            best_vNodeToPNode = new LinkedHashMap<>(vNodeToPNode);
                        }
                    }
                }
                //release reserved for ve resources to try another mapping
                releaseVEdgeResources(metaPath.getLeft(), vNodeToPNode, vLinkToPath, survivedVNs, survivedVEs);
            }
            ctrlMessages++;
            checkedNodes.removeAll(checkedNodesInt);
            return;

        } else
            throw new RuntimeException("Iteration has been exceeded!"); //todo make your own exception!
    }

    private List<Pair<VirtualLink, List<INode>>> genMetapath(String vSrc, Map<String, List<VirtualLink>> vnNhs,
                                                                  ITopology t, Map<String, INode> vNodeToPNode,
                                                                  Map<VirtualLink, List<INode>> vLinkToPath,
                                                                  int maxMetapaths, boolean isDynamic, int depth) {
        String vDst = null;
        VirtualLink ve = null;
        for (VirtualLink vl : vnNhs.get(vSrc))
            if (!vLinkToPath.containsKey(vl)) {
                ve = vl;
                vDst = ve.getSrc().equals(vSrc) ? ve.getDst() : ve.getSrc();
                break;
            }
        if (ve == null || vDst == null)
            return Collections.EMPTY_LIST;
        //calculate metapath num decay
        //int mNum = Math.max(1, (int) Math.round(maxMetapaths * (1 - Math.log(depth) / Math.log(maxMetapaths))));
        int mNum = (int) Math.round(Math.pow(maxMetapaths, 1.0 / depth));
        //choose correct direction of ve
        vSrc = ve.getSrc().equals(vSrc) ? ve.getSrc() : ve.getDst();
        vDst = ve.getSrc().equals(vDst) ? ve.getSrc() : ve.getDst();
        List<Pair<VirtualLink, List<INode>>> possibleMetapaths = new ArrayList<>();

        //pick pre-computed metapaths
        if (!isDynamic) { //0 ctrl messages are needed
            for (MPath candidate : ve.getMpCandidates())
                if (candidate.getPath().size() > 2) {
                    List<INode> path = new ArrayList<>(candidate.getPath().size());
                    path.addAll(candidate.getPath());
                    //todo: check what happens is nodes are unknown by the controller!!!
                    if (validatePath(ve, path)) {
                        //System.out.println("[DEBUG] [MpSC Pre-computed] metapath cost=" + candidate.second() + " mpaths # of edges=" + path.size() + " ve=" + ve);
                        possibleMetapaths.add(new ImmutablePair<>(ve, path));
                    }
                    if (possibleMetapaths.size() == mNum)
                        break;
                }
        } else {
            //TODO: add dynamic policy later
            /*
            //find them dynamically
            //System.out.println("[DEBUG] [MpSC Pre-computed] current depth=" + depth + " # of mpaths " + mNum + " ve=" + ve);
            //set cost to all pedges
            for (Edge pedge : g.getEdgesArray())
                pedge.setCost(ve.getBW() / pedge.getAvBW());
            //create metanodes
            clearMetaedges(g);
            Node mSrc = convertToMetanode(true, vSrc, pSrcs);
            Node mDst = convertToMetanode(false, vDst, pDsts);
            try {
                List<Pair<List<INode<T>>, Double>> mpaths = kBestNM.findKOptimalPathWithConstraints(mSrc, mDst, g, ve.getBW(), 0, ve.R, ve.getDelay(), mNum, 64);
                ctrlMessages += kBestNM.getCtrlMessageNum();
                for (Pair<List<Node>, Double> candidate : mpaths) {
                    List<Node> path = candidate.first();
                    //System.out.println("[DEBUG] [MpSC Pre-computed] metapath cost=" + candidate.second() + " mpaths # of edges=" + path.size() + " ve=" + ve);
                    possibleMetapaths.add(new Pair<>(ve, path.subList(1, path.size() - 1)));
                }
            } catch (Throwable ex) {
            }
            //remove metaedges
            clearMetaedges(g);
//            for (Node pSrc : pSrcs)
//                for (Node pDst : pDsts)
//                    if (!pSrc.equals(pDst)) { //todo potentially release this constraint!
//                        //todo switch to NM
//                        try {
//                            List<Node> path = dijkstra.computeBwCostPath(pSrc.getID(), pDst.getID(), ve.getBW(),
//                                    Double.MAX_VALUE, false).keySet().iterator().next();
//                            possibleMetapaths.add(new Pair(ve, path));
//                        } catch (Exception ex) {
//                        }
//                    }
//
//            //sort and keep best
//            Collections.sort(possibleMetapaths, metapathCostComparator);
//            if (maxMetapaths < possibleMetapaths.size())
//                possibleMetapaths = possibleMetapaths.subList(0, maxMetapaths); //keep best metapaths
            */
        }
        return possibleMetapaths;
    }

    private void allocateMetapath(Pair<VirtualLink, List<INode>> metaPath,
                                      Map<String, INode> vNodeToPNode,
                                      Map<VirtualLink, List<INode>> vLinkToPath) {
        VirtualLink ve = metaPath.getLeft();
        List<INode> path = metaPath.getRight();

        /* TODO: properly allocate resources to host a metapath
        //allocate CPU for vSrc and vDst first
        if (!vNodeToPNode.containsKey(ve.getSrc())) {
            ctrlMessages++; //provision vm
            vNodeToPNode.put(ve.getSrc(), path.get(0));
            path.get(0).setStress(path.get(0).getStress() + ve.getSrc().getTarget());
        }
        if (!vNodeToPNode.containsKey(ve.getDst())) {
            ctrlMessages++; //provision vm
            vNodeToPNode.put(ve.getDst(), path.get(path.size() - 1));
            path.get(path.size() - 1).setStress(path.get(path.size() - 1).getStress() + ve.getDst().getTarget());
        }

        //allocate BW for VE then
        if (!vLinkToPath.containsKey(ve)) {
            vLinkToPath.put(ve, convertPath(path));
            for (int i = 0; i < path.size() - 1; i++) {
                ctrlMessages++; //setup flow
                path.get(i).getNeighbors().get(path.get(i + 1)).allocateResources(ve.getBW());
            }
        }
        */
    }

    private String getVRoot(List<VirtualLink> vls) {
        //TODO extend RestAPI to know about root service!
        /*
        if (vls != null && !vls.isEmpty()) {
            VirtualLink vl = vls.get(vls.size() - 1);
            if (isRoot(vl.getSrc()))
                return vl.getSrc();
            else if (isRoot(vl.getDst()))
                return vl.getDst();
        }
        */
        return null;
    }


    private boolean correctMapping(List<VirtualLink> vls, Map<String, INode> vNodeToPNode,
                                   Map<VirtualLink, List<INode>> vLinkToPath) {
        /*TODO: fix correct mapping function
        //check mapping for vnodes
        for (Node vnode : vnr.getNodes())
            if (!vNodeToPNode.containsKey(vnode))
                return false;

        //check mapping for vedges
        for (Edge vedge : vnr.getEdges())
            if (!vLinkToPath.containsKey(vedge))
                return false;
        */
        return true;
    }


    private void releaseAllVNRResourcesBeenUsed(Map<String, INode> vNodeToPNode,
                                                    Map<VirtualLink, List<INode>> vLinkToPath) {
        /* TODO: finilize releaseAllVNR resources
        //remove allocated VNodes
        for (Map.Entry<Node, Node> nodeEntry : vNodeToPNode.entrySet()) {
            ctrlMessages++;
            nodeEntry.getValue().setStress(nodeEntry.getValue().getStress() - nodeEntry.getKey().getTarget());
        }

        //remove allocated VLinks
        for (Map.Entry<Edge, List<Edge>> linkEntry : vLinkToPath.entrySet())
            for (Edge pe : linkEntry.getValue()) {
                ctrlMessages++;
                pe.allocateResources(-linkEntry.getKey().getBW());
            }
            */
        vNodeToPNode.clear();
        vLinkToPath.clear();
    }

    private void releaseVEdgeResources(VirtualLink vl,
                                       Map<String, INode> vNodeToPNode,
                                       Map<VirtualLink, List<INode>> vLinkToPath, Set<String> survivedVNs, Set<VirtualLink> survivedVEs) {
        /* TODO: finalize release VEdge resources function
        //release CPU for vSrc and vDst first
        Node pSrc = survivedVNs.contains(vl.getSrc()) ? null : vNodeToPNode.remove(vl.getSrc());
        if (pSrc != null) {
            ctrlMessages++; //release vm resources
            pSrc.setStress(pSrc.getStress() - vl.getSrc().getTarget());
        }
        Node pDst = survivedVNs.contains(vl.getDst()) ? null : vNodeToPNode.remove(vl.getDst());
        if (pDst != null) {
            ctrlMessages++; //release vm resources
            pDst.setStress(pDst.getStress() - vl.getDst().getTarget());
        }
        //release BW for VE then
        //cannot have a survived edge here!
        List<Edge> path = survivedVEs.contains(vl) ? null : vLinkToPath.remove(vl);
        if (path != null)
            for (Edge pe : path) {
                ctrlMessages++; //remove flow
                pe.allocateResources(-vl.getBW());
            }
            */
    }

    private void allocateVNR(List<VirtualLink> vnr, Map<String, INode> vNodeToPNode,
                             Map<VirtualLink, List<INode>> vLinkToPath) {
        /*
        //allocate resources for vnodes
        for (Node vnode : vnr.getNodes())
            if (vNodeToPNode.containsKey(vnode)) {
                ctrlMessages++;//allocate vm
                Node pnode = vNodeToPNode.get(vnode);
                double newStress = pnode.getStress() + vnode.getTarget();
                if (newStress <= pnode.getTarget())
                    pnode.setStress(newStress);
                else
                    throw new RuntimeException("[MpSM] Inconsistency detected during VNodes allocation!:\nVNode id="
                            + vnode.getID() + " with target=" + vnode.getTarget() + " cannot be mapped to PNode id="
                            + pnode.getID() + " with avTarget=" + (pnode.getTarget() - pnode.getStress()));
            } //else
        //  throw new RuntimeException("[PathVNE] Inconsistency detected - no mapping for VNode id=" + vnode.getID());


        //allocate resources for vedges
        for (Edge vedge : vnr.getEdges())
            if (vLinkToPath.containsKey(vedge))
                allocateVLink(vedge, vLinkToPath.get(vedge));
        //else
        //    throw new RuntimeException("[PathVNE] Inconsistency detected - no mapping for VEdge " + vedge);
        */
    }

    private void allocateVLink(VirtualLink vl, List<String> path) {
        //TODO: think on how to allocate VL
        /*
        for (Edge pe : path) {
            ctrlMessages++; //push the flow
            if (ve.getBW() > pe.getAvBW())
                System.out.println("Gotcha!!!");
            pe.allocateResources(ve.getBW());
        }
        */
    }

    private boolean validatePath(VirtualLink vl, List<INode> path) {
        //TODO: think on how to validate a path
        /*
        double minBW = Double.MAX_VALUE;
        double delay = 0;
        for (Edge e : path) {
            if (e.isFailed())
                return false;
            delay += e.getDelay();
            if (e.getAvBW() < minBW)
                minBW = e.getAvBW();
        }
        if (minBW >= ve.getBW() && delay <= ve.getDelay())
            return true;
        else
        */
        return false;
    }

    private void clearMetaedges(ITopology t) {
        /* TODO: finilize clear metaedges function
        for (Node n : g.getNodesArray()) {
            Set<Node> nhToRemove = new HashSet<>();
            for (Node nh : n.getNeighbors().keySet())
                if (nh.isMeta())
                    nhToRemove.add(nh);

            n.getNeighbors().keySet().removeAll(nhToRemove);
        }
        */
    }

    //at this point we calculate objective with assumption that resources were already allocated!
    public double calculateVNRObjective(Map<String, INode> vNodeToPNode, Map<VirtualLink, List<INode>> vLinkToPath) {
        double obj = 0;
        /*TODO: finilize calculateVNR objective
        Map<VirtualLink, Double> peAllocs = new HashMap<>(vLinkToPath.size());
        for (Map.Entry<VirtualLink, List<INode<T>>> e : vLinkToPath.entrySet())
            for (Edge pe : e.getValue())
                peAllocs.put(pe, (peAllocs.containsKey(pe) ? peAllocs.remove(pe) : 0)
                        + e.getKey().getBW());

        Map<Node, Double> pnAllocs = new HashMap<>(vLinkToPath.size());
        for (Map.Entry<Node, Node> e : vNodeToPNode.entrySet())
            pnAllocs.put(e.getValue(), (pnAllocs.containsKey(e.getValue()) ? pnAllocs.remove(e.getValue()) : 0)
                    + e.getKey().getTarget());

        //link mapping objective
        for (Map.Entry<Edge, List<Edge>> e : vLinkToPath.entrySet())
            for (Edge pe : e.getValue())
                obj += e.getKey().getBW() / (pe.getAvBW() + peAllocs.get(pe));

        //node mapping objective
        for (Map.Entry<Node, Node> e : vNodeToPNode.entrySet())
            obj += e.getKey().getTarget() / (e.getValue().getTarget() - e.getValue().getStress() + pnAllocs.get(e.getValue()));
        */
        return obj;
    }

    private Map<String, List<VirtualLink>> getVirtualNeighbors(List<VirtualLink> vls) {
        Map<String, List<VirtualLink>> vnNhs = new HashMap<>();
        for (VirtualLink vl : vls) {
            if (!vnNhs.containsKey(vl.getSrc()))
                vnNhs.put(vl.getSrc(), new ArrayList<>());
            vnNhs.get(vl.getSrc()).add(vl);
            if (!vnNhs.containsKey(vl.getDst()))
                vnNhs.put(vl.getDst(), new ArrayList<>());
            vnNhs.get(vl.getDst()).add(vl);
        }
        return vnNhs;
    }

}

/**
 * NodeCost comparator sorts in ascending order virtual to physical node mapping costs (should be inline with the objective, i.e., min vs max).
 */
class MetapathCostComparator implements Comparator {

    public int compare(Object o1, Object o2) {
        //estimate first metapath cost
        Pair<VirtualLink, List<INode>> metapath1 = (Pair<VirtualLink, List<INode>>) o1;
        VirtualLink ve1 = metapath1.getLeft();
        List<INode> path1 = metapath1.getRight();
        double cost1 = estimateMetapathCost(ve1, path1);

        //estimate second metapath cost
        Pair<VirtualLink, List<INode>> metapath2 = (Pair<VirtualLink, List<INode>>) o2;
        VirtualLink ve2 = metapath2.getLeft();
        List<INode> path2 = metapath2.getRight();
        double cost2 = estimateMetapathCost(ve2, path2);

        //compare both costs
        if (cost1 < cost2) return -1;
        if (cost1 == cost2) return 0;
        if (cost1 > cost2) return 1;

        return 1;  //never gets here but javac complains
    }

    /**
     * By convention the vSrc of the ve is to be mapped onto the pSrc of the path
     *
     * @param ve   - virtual edge
     * @param path - physical path
     * @return cost of mapping vedge + vnodes of this vedge to the physical path and src/dst physical nodes
     */
    private double estimateMetapathCost(VirtualLink ve, List<INode> path) {
        double cost = ve.getSrcCap() / (path.get(0).getCapacity() - path.get(0).getStress())
                + ve.getDstCap() / (path.get(path.size() - 1).getCapacity() - path.get(path.size() - 1).getStress());

        for (int i = 0; i < path.size() - 1; i++) {
            ILink link = (ILink) path.get(i).getNeighbors().get(path.get(i + 1));
            cost += ve.getBw() / link.getBw();
        }

        return cost;
    }
}

/**
 * NodeCost comparator sorts in ascending order virtual to physical node mapping costs (should be inline with the objective, i.e., min vs max).
 */
class NodeCostComparator implements Comparator {
    public int compare(Object n1, Object n2) {
        Pair<String, INode> v1n1 = (Pair<String, INode>) n1;
        Pair<String, INode> v2n2 = (Pair<String, INode>) n2;
        /*
        double newR1 = v1n1.first().R < (1 - v1n1.second().getRisk()) ?
                v1n1.first().R / (1 - v1n1.first().getRisk()) : v1n1.first().R;
        double demand1 = v1n1.first().targetSigma == 0 || v1n1.first().R == 0 ? v1n1.first().getTarget() :
                calculateDemand(v1n1.first().getTarget(), v1n1.first().targetSigma, newR1);
        */
        double demand1 = 0;
        double n1Cost = demand1 / (v1n1.getRight().getCapacity() - v1n1.getRight().getStress());

        /*
        double newR2 = v2n2.first().R < (1 - v2n2.second().getRisk()) ?
                v2n2.first().R / (1 - v2n2.first().getRisk()) : v2n2.first().R;
        double demand2 = v2n2.first().targetSigma == 0 || v2n2.first().R == 0 ? v2n2.first().getTarget() :
                calculateDemand(v2n2.first().getTarget(), v2n2.first().targetSigma, newR2);
        */
        double demand2 = 0; //TODO: fix VNode demand estimation
        double n2Cost = demand2 / (v2n2.getRight().getCapacity() - v2n2.getRight().getStress());

        if (n1Cost < n2Cost) return -1;
        if (n1Cost == n2Cost) return 0;
        if (n1Cost > n2Cost) return 1;

        return 1;  //never gets here but javac complains
    }
}