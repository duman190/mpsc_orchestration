package net.floodlightcontroller.mpsc.utils;

import net.floodlightcontroller.mpsc.xml.GeneralConfigParser;
import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;
import net.floodlightcontroller.mpsc.MPSCConstants;
import net.floodlightcontroller.statistics.IStatisticsService;
import net.floodlightcontroller.statistics.SwitchPortBandwidth;
import org.projectfloodlight.openflow.protocol.OFActionType;
import org.projectfloodlight.openflow.protocol.OFFlowMod;
import org.projectfloodlight.openflow.protocol.action.OFAction;
import org.projectfloodlight.openflow.protocol.action.OFActionEnqueue;
import org.projectfloodlight.openflow.types.DatapathId;
import org.projectfloodlight.openflow.types.OFPort;

import java.util.Map;

/**
 * Created by chemo_000 on 4/29/2015.
 */
public class AllocatedBwEstimator implements MPSCConstants {
    /**
     * @param dpid       - dpid of src switch
     * @param port       - src port
     * @param flowPusher - IStaticFlowPusherService of Floodlight
     * @return - allocated bw on @dpid switch and specified @port
     */
    public double estimateAllocatedBw(DatapathId dpid, short port,
                                      IStaticFlowEntryPusherService flowPusher,
                                      IStatisticsService stat) {
        double allocatedCost = 0;

        if (GeneralConfigParser.getInstance().ENABLE_QOS) {
            Map<String, OFFlowMod> flows = flowPusher.getFlows(dpid);

            if (flows != null)
                for (OFFlowMod flow : flows.values())
                    for (OFAction action : flow.getActions())
                        if (action.getType().equals(OFActionType.ENQUEUE)) {
                            OFActionEnqueue queue = (OFActionEnqueue) action;

                            if (queue.getPort().getShortPortNumber() == port)
                                //determine latency by queue id and queue id/latency ratio
                                allocatedCost += queue.getQueueId() * BW_TO_QUEUE_ID_RATIO;
                        }

        } else {
            System.out.println("[DEBUG-MPSC-Stat] Statistic service is enabled:" + stat);
            //TODO: doesn't work -> raise null pointer exception
            double rxSpeedMbps = 0;
            double txSpeedMbps = 0;

            try {
                if (port >= 0) {
                    SwitchPortBandwidth swp = stat.getBandwidthConsumption(dpid, OFPort.of(port));
                    System.out.println("[DEBUG-MPSC-Stat] The following statistic has been collected for switch dpid="
                            + dpid + ", port=" + port + ":" + swp);
                    long Rx = swp.getBitsPerSecondRx().getValue();
                    long Tx = swp.getBitsPerSecondRx().getValue();
                    System.out.println("[DEBUG-MPSC-Stat] The following Rx and Tx speeds [in bps] has been collected for switch dpid="
                            + dpid + ", port=" + port + ": rx=" + Rx + ", tx=" + Tx);
                    rxSpeedMbps = new Long(Rx).doubleValue() / 1000000;
                    txSpeedMbps = new Long(Tx).doubleValue() / 1000000;
                }
            } catch (Exception ex) {
                System.out.println("[DEBUG-MPSC-Stat] Next error happened during switch dpid="
                        + dpid + ", port=" + port + " statistic collection:" + ex.getMessage());
                ex.printStackTrace();
            }

            allocatedCost = Math.max(rxSpeedMbps, txSpeedMbps);
        }
        return allocatedCost;
    }
}
