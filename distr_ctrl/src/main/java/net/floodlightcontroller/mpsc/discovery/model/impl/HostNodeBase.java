package net.floodlightcontroller.mpsc.discovery.model.impl;

import net.floodlightcontroller.mpsc.openflow.customservices.impl.HostHolderImpl;
import net.floodlightcontroller.mpsc.xml.TopoConfigParser;
import net.floodlightcontroller.mpsc.discovery.model.ILink;
import net.floodlightcontroller.mpsc.discovery.model.INode;
import org.projectfloodlight.openflow.types.DatapathId;
import org.projectfloodlight.openflow.types.IPv4Address;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by chemo_000 on 2/10/2015.
 */
public class HostNodeBase<T> extends NodeBaseAbstract
{
    private T switchID;
    private short port;


    public HostNodeBase(T id, T switchID, short port, TopologyBase topology)
    {
        super(id, topology);
        this.switchID = switchID;
        this.port = port;
    }

    @Override
    public double getCapacity() {
        return TopoConfigParser.getInstance().getNodeCapacity(this.getID().toString());
    }

    @Override
    public double getStress() {
        Double stress = HostHolderImpl.getInstance().getHostAllocatedResources(IPv4Address.of(this.id.toString()));
        if (stress != null)
            return stress;
        else
            return getCapacity(); //allocated resources are unknown; assume that fully utilized!
    }

    @Override
    public void setStress(double stress) {
        HostHolderImpl.getInstance().setHostAllocatedResources(IPv4Address.of(this.id.toString()), stress);
    }

    @Override
    public INodeType getType()
    {
        return INodeType.HOST;
    }

    @Override
    public Map<INode<T>, ILink> getNeighbors()
    {
        if (!neighbors.isEmpty())
            return neighbors;
        else
        {

            Map<INode<T>, ILink> neighbors = new HashMap<>();

            if (switchID instanceof String)
            {
                INode<T> directSwitch = new SwitchNodeBase<>(switchID, topology);

                ILink hostLink = new HostLinkBase(topology, this.getID().toString(), DatapathId.of((String) switchID), port);

                neighbors.put(directSwitch, hostLink);
            }

            if (log.isTraceEnabled())
                log.trace("[NM-model]" + this + " has next neighbors" + neighbors);

            return neighbors;
        }
    }
}
