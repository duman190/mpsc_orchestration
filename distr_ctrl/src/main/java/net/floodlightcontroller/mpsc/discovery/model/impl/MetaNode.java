package net.floodlightcontroller.mpsc.discovery.model.impl;

import net.floodlightcontroller.mpsc.discovery.model.ILink;
import net.floodlightcontroller.mpsc.discovery.model.INode;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by dmitriichemodanov on 2/28/18.
 */
public class MetaNode<T> extends NodeBaseAbstract {

    private double capacity;

    public MetaNode(T id, double capacity)
    {
        super(id, null);

        this.capacity = capacity;
    }

    @Override
    public double getStress() {
        return this.capacity; //stress of metanodes is always equal to capacity
    }

    @Override
    public void setStress(double stress) {
    }

    @Override
    public INodeType getType() {
        return INodeType.META_NODE;
    }

    @Override
    public Map<INode, ILink> getNeighbors() {
        return neighbors;
    }
}
