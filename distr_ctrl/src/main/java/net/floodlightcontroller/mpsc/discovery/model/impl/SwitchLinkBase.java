package net.floodlightcontroller.mpsc.discovery.model.impl;

import net.floodlightcontroller.routing.Link;
import net.floodlightcontroller.mpsc.discovery.model.INode;
import net.floodlightcontroller.mpsc.xml.TopoConfigParser;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.projectfloodlight.openflow.types.DatapathId;

/**
 * Created by chemo_000 on 2/10/2015.
 */
public class SwitchLinkBase extends LinkBaseAbstract {
    private Link link;

    public SwitchLinkBase(TopologyBase topology, Link link) {
        super(topology);

        this.link = link;
    }

    @Override
    //@todo find a way to obtain capacity dynamically
    public double getBw() {
        DatapathId srcDpid = link.getSrc();
        short srcPort = link.getSrcPort().getShortPortNumber();
        double srcUtil = calculateAllocatedBwForPort(srcDpid, srcPort);
        DatapathId dstDpid = link.getDst();
        short dstPort = link.getDstPort().getShortPortNumber();
        double dstUtil = calculateAllocatedBwForPort(dstDpid, dstPort);

        Pair<String, String> key = new ImmutablePair<>(link.getSrc().toString(), link.getDst().toString());
        Double capacity = TopoConfigParser.getInstance().getLinkCapacity(key);
        //as we use undirected VL we do need worry about direction and calculate bw
        //for src port; bw for dst port will be the same
        return (capacity == null ? MAX_SWITCH_LINK_BW : capacity) - Math.max(srcUtil, dstUtil);
    }

    @Override
    public double getLatency() {
        Pair<String, String> key = new ImmutablePair<>(link.getSrc().toString(), link.getDst().toString());
        Double latency = TopoConfigParser.getInstance().getLinkLatency(key);
        return latency == null ? 0 : latency;
    }

    @Override
    public <T> short getSrcPort(INode<T> src, INode<T> dst) {
        if (src.getType() == INode.INodeType.SWITCH &&
                (src.getID() instanceof DatapathId || src.getID() instanceof String)) {
            DatapathId srcDPID;

            if (src.getID() instanceof DatapathId)
                srcDPID = (DatapathId) src.getID();
            else
                srcDPID = DatapathId.of((String) src.getID());

            return link.getSrc().equals(srcDPID) ?
                    link.getSrcPort().getShortPortNumber() :
                    link.getDstPort().getShortPortNumber();
        } else if (src.getType() == INode.INodeType.HOST) {
            return 0;
        } else
            throw new UnsupportedOperationException(src.getID().getClass() +
                    "[NM-model] type of node ID currently isn't supported!");
    }

    public String toString() {
        return "[NM-model][" + link + "] bw=" + getBw();
    }
}
