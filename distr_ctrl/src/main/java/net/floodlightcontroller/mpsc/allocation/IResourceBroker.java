package net.floodlightcontroller.mpsc.allocation;

import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;
import net.floodlightcontroller.mpsc.MPSCConstants;
import net.floodlightcontroller.mpsc.discovery.model.ILink;
import net.floodlightcontroller.mpsc.discovery.model.INode;

/**
 * Created by chemo_000 on 2/6/2015.
 */
public interface IResourceBroker extends MPSCConstants
{
    public <T> void allocateSpecifiedBandwidth(INode<T> src, INode<T> dst, INode<T> current, INode<T> next, ILink link, double cost);

    public <T> void allocateSpecifiedBandwidth(String src, String dst, INode<T> current, INode<T> next, ILink link, double cost);

    public <T> void releaseSpecifiedBandwidth(INode<T> src, INode<T> dst, INode<T> current, INode<T> next, ILink link, double cost);

    public <T> void releaseSpecifiedBandwidth(String src, String dst, INode<T> current, INode<T> next, ILink link, double cost);

    public void setFlowPusher(IStaticFlowEntryPusherService flowPusher);

    public boolean needFlowPusher();
}
