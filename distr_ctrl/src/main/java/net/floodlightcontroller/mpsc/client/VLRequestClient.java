package net.floodlightcontroller.nm.client;

import java.io.*;
import java.net.*;
import java.util.Scanner;

/**
 * Created by chemo_000 on 3/9/2015.
 */
public class VLRequestClient
{
    public static void main(String[] args)
    {
        InputStreamReader convert = new InputStreamReader(System.in);
        BufferedReader stdin = new BufferedReader(convert);

        System.out.println("[NM-Client]Started");

        String address = null;

        Socket s = null;
        try
        {
            System.out.println("[NM-Client]Please, input server address...");
            address = stdin.readLine();

            s = new Socket(address, 42015);// open TCP socket (for UDP please use DatagramSocket class)
            System.out.println("[NM-Client]Successfully connected to " + address + " port=" + 42015);
            PrintStream outs = new PrintStream(s.getOutputStream()); //create output  stream
            BufferedReader ins = new BufferedReader(new InputStreamReader(s.getInputStream()));// create input stream

            boolean repeat = true;
            Scanner sc = new Scanner(System.in);
            String line = "";
            while (repeat)
            {
                //specify VL
                //src ip
                System.out.println(ins.readLine());
                outs.println(sc.next());
                //dst ip
                System.out.println(ins.readLine());
                outs.println(sc.next());
                //bw
                System.out.println(ins.readLine());
                outs.println(sc.nextDouble());
                //splittable
                System.out.println(ins.readLine());
                outs.println(sc.nextBoolean());

                //check for exception
                line = ins.readLine();

                if (line.equals("ERROR"))
                    System.out.println(line + ":" + ins.readLine());
                else
                {
                    //print result
                    System.out.println(line);
                    boolean res = sc.nextBoolean();
                    outs.println(res);
                    if (res)
                    {
                        System.out.println(ins.readLine());
                        System.out.println(ins.readLine());
                    }

                    //delete?
                    System.out.println(ins.readLine());
                    boolean delete = sc.nextBoolean();
                    outs.println(delete);
                    if (delete)
                        System.out.println(ins.readLine());
                }

                //repeat?
                System.out.println(ins.readLine());
                outs.println(sc.nextBoolean());
            }
        } catch (UnknownHostException ex)
        {
            System.err.println("[NM-Client]Can't login to server: " + address + ". Please check address!");
        } catch (IOException e)
        {
            System.err.println("[NM-Client]Next exception occurred during sending: "
                    + e + "! Smth wrong with server. Connection was closed.");
        } finally
        {
            if (s != null)
                try
                {
                    s.close();
                } catch (Exception ex)
                {
                }

            System.err.println("[NM-Client]Socket has been closed!");
        }
    }
}
