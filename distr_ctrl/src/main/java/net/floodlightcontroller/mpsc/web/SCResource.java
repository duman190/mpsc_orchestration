package net.floodlightcontroller.mpsc.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.floodlightcontroller.mpsc.discovery.model.ILink;
import net.floodlightcontroller.mpsc.discovery.model.INode;
import net.floodlightcontroller.mpsc.mapping.optimization.MetapathOpt;
import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;
import net.floodlightcontroller.mpsc.allocation.RouteBroker;
import net.floodlightcontroller.mpsc.openflow.customservices.IRouteFinderService;
import net.floodlightcontroller.mpsc.openflow.customservices.impl.VLHolder;
import net.floodlightcontroller.mpsc.utils.LogWriter;
import net.floodlightcontroller.mpsc.web.model.VirtualLink;
import net.floodlightcontroller.mpsc.xml.GeneralConfigParser;
import org.apache.commons.lang3.tuple.Pair;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import java.io.IOException;
import java.util.*;

/**
 * Created by chemo_000 on 4/23/2015.
 */
public class SCResource extends ServerResource implements IJsonVL {
    @Get("json")
    public List<VirtualLink> retrieve() {
        String user = getAttribute("user");
        List<VirtualLink> res = null;

        if (user != null)
            res = VLHolder.getInstance().getUserVL(user);

        if (res != null)
            return res;
        else
            return Collections.EMPTY_LIST;
    }

    @Post("json")
    public String allocateSC(String json) {
        IStaticFlowEntryPusherService flowPusher =
                (IStaticFlowEntryPusherService) getContext().getAttributes().
                        get(IStaticFlowEntryPusherService.class.getCanonicalName());

        IRouteFinderService routeFinder =
                (IRouteFinderService) getContext().getAttributes().
                        get(IRouteFinderService.class.getCanonicalName());

        if (RouteBroker.getInstance().needFlowPusher())
            RouteBroker.getInstance().setFlowPusherService(flowPusher);

        String user = getAttribute("user");

        if (user != null)
            try {
                List<VirtualLink> vls = jsonToVL(json);

                //TODO: verify SC request

                //Compose SC
                Pair<Map<String, INode>, Map<VirtualLink, List<INode<String>>>> scMappings;
                String compositionEx = "";
                try {
                    //TODO: parametrize number of metapath (= # of nodes by default)
                    scMappings = new MetapathOpt(routeFinder).mapServiceChain(vls,
                            routeFinder.getTopo(), routeFinder.getTopo().getKnownNodes().size());
                } catch (Exception ex) {
                    compositionEx = ex.getMessage();
                    scMappings = null;
                }

                if (scMappings != null) {
                    boolean provisionContainters = false;
                    //allocate SC services
                    if (provisionContainters) {
                        //TODO: provision containers and assign their IP to each service
                    } else {
                        Set<INode> allocatedPNs = new HashSet<>();
                        //assign bare metal server IP to each service if no containers are used
                        for (VirtualLink vl : vls) {
                            //retrieve corresponding physical nodes
                            INode pSrc = scMappings.getLeft().get(vl.getSrc());
                            INode pDst = scMappings.getLeft().get(vl.getDst());
                            //assign IP addresses
                            vl.setSrcIP(pSrc.getID().toString());
                            vl.setDstIP(pDst.getID().toString());
                            //allocate physical node resources
                            if (!allocatedPNs.contains(pSrc)) {
                                pSrc.setStress(pSrc.getStress() + vl.getSrcCap());
                                allocatedPNs.add(pSrc);
                            }
                            if (!allocatedPNs.contains(pDst)) {
                                pDst.setStress(pDst.getStress() + vl.getDstCap());
                                allocatedPNs.add(pDst);
                            }
                        }
                    }

                    //allocate corresponding SC flows
                    for (VirtualLink vl : vls)
                        try {
                            long startPathQuery = System.nanoTime();
                            //routeFinder.findRoutesAndBwCost(vl.getSrc(), vl.getDst(), false, vl.getBw(), vl.getLatency());
                            routeFinder.allocateRoute(scMappings.getRight().get(vl), vl.getBw());
                            vl.setStatus(VirtualLink.Status.ALLOCATED);
                            if (GeneralConfigParser.getInstance().EXPORT_RESULTS)
                                LogWriter.getInstance().logInfoToFile((System.nanoTime() - startPathQuery) / 1000 + "\n");
                        } catch (Exception ex) {
                            vl.setStatus(VirtualLink.Status.FAILED);
                            vl.setError(ex.getMessage());
                            ex.printStackTrace();
                        }
                } else //sc composition has failed! Report back the result!
                {
                    for (VirtualLink vl : vls) {
                        vl.setStatus(VirtualLink.Status.FAILED);
                        vl.setError(compositionEx);
                    }
                }

                VLHolder.getInstance().addUserVL(user, vls);
                return vlToJson(vls);
            } catch (Exception ex) {
                ex.printStackTrace();
                return ("{\"ERROR\" : \"exception during service chain composition: " + ex.getMessage() + "\"}");
            }
        else
            return ("{\"ERROR\" : \"user=null\"}");
    }

    public static List<VirtualLink> jsonToVL(String json) throws IOException {
        System.out.println("Next JSON obtained: " + json);
        ObjectMapper mapper = new ObjectMapper();
        //deserialize JSON objects as a Set
        List<VirtualLink> vls = mapper.readValue(json,
                mapper.getTypeFactory().constructCollectionType(List.class, VirtualLink.class));
        return vls;
    }

    public static String vlToJson(List<VirtualLink> vls) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(vls);

        return json;
    }
}
