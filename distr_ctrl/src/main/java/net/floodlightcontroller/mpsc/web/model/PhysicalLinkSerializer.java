package net.floodlightcontroller.mpsc.web.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import net.floodlightcontroller.mpsc.web.IJsonVL;

import java.io.IOException;

/**
 * Created by chemo_000 on 4/29/2015.
 */
public class PhysicalLinkSerializer extends JsonSerializer<PhysicalLink> implements IJsonVL
{
    @Override
    public void serialize(PhysicalLink pl, JsonGenerator jgen, SerializerProvider arg) throws IOException, JsonProcessingException
    {
        jgen.writeStartObject();
        jgen.writeStringField(SRC, pl.src);
        jgen.writeStringField(SRC_TYPE, pl.srcType.toString());
        jgen.writeNumberField(SRC_CAP, pl.srcCap);
        jgen.writeNumberField(SRC_STRESS, pl.srcStress);
        jgen.writeStringField(DST, pl.dst);
        jgen.writeStringField(DST_TYPE, pl.dstType.toString());
        jgen.writeNumberField(DST_CAP, pl.dstCap);
        jgen.writeNumberField(DST_STRESS, pl.dstStress);
        jgen.writeNumberField(BW, pl.avBw);
        jgen.writeNumberField(CAPACITY, pl.capacity);
        jgen.writeNumberField(LATENCY, pl.latency);
        jgen.writeEndObject();
    }
}
