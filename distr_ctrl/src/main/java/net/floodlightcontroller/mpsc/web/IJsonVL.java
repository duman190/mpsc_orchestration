package net.floodlightcontroller.mpsc.web;

/**
 * Created by chemo_000 on 4/27/2015.
 */
public interface IJsonVL
{
    public final String SRC = "source";
    public final String SRC_IP = "srcIP";
    public final String SRC_DEMAND = "src_demand";
    public final String SRC_TYPE = "src_type";
    public final String SRC_CAP = "src_cap";
    public final String SRC_STRESS = "src_stress";
    public final String DST = "destination";
    public final String DST_IP = "dstIP";
    public final String DST_DEMAND = "dst_demand";
    public final String DST_TYPE = "dst_type";
    public final String DST_CAP = "dst_cap";
    public final String DST_STRESS = "dst_stress";
    public final String BW = "bandwidth";
    public final String CAPACITY = "capacity";
    public final String LATENCY = "latency";
    public final String STATUS = "status";
    public final String ERROR = "error";
    public final String SWITCH_RULES = "switch_rules";
    public final String MP_CANDIDATES = "mp_candidates";
    public final String PATH = "path";
    public final String COST = "cost";
}
