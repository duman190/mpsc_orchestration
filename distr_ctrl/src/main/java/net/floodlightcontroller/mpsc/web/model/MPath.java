package net.floodlightcontroller.mpsc.web.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import net.floodlightcontroller.mpsc.discovery.model.INode;

import java.util.List;

/**
 * Created by dmitriichemodanov on 10/12/18.
 */
@JsonSerialize(using = MPathSerializer.class)
@JsonDeserialize(using = MPathDeserializer.class)
public class MPath {
    protected String src;
    protected String dst;
    protected List<INode<String>> path; //TODO: add INode Serializer/Deserializer
    protected double cost;

    public MPath (String src, String dst, List<INode<String>> path, double cost)
    {
        this.src = src;
        this.dst = dst;
        this.path = path;
        this.cost = cost;
    }

    public String getSrc()
    {
        return this.src;
    }

    public String getDst()
    {
        return this.dst;
    }

    public List<INode<String>> getPath()
    {
        return this.path;
    }

    public double getCost()
    {
        return this.cost;
    }
}
