package net.floodlightcontroller.mpsc.web.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import net.floodlightcontroller.mpsc.web.IJsonVL;

import java.io.IOException;

/**
 * Created by chemo_000 on 4/27/2015.
 */
public class VirtualLinkDeserializer extends JsonDeserializer<VirtualLink> implements IJsonVL {

    @Override
    public VirtualLink deserialize(JsonParser jp, DeserializationContext dC) throws IOException, JsonProcessingException {
        String src = null;
        String srcIP = "-";
        String dst = null;
        String dstIP = "-";
        double src_demand = 0;
        double dst_demand = 0;
        double bw = 0;
        double latency = 0;
        VirtualLink.Status status = null;
        String error="";

        if (jp.getCurrentToken() != JsonToken.START_OBJECT) {
            throw new IOException("Expected START_OBJECT");
        }

        while (jp.nextToken() != JsonToken.END_OBJECT) {
            if (jp.getCurrentToken() != JsonToken.FIELD_NAME) {
                throw new IOException("Expected FIELD_NAME");
            }

            String n = jp.getCurrentName();
            jp.nextToken();
            if (jp.getText().equals("")) {
                continue;
            }

            switch (n) {
                case SRC:
                    src = jp.getText();
                    break;
                case SRC_IP:
                    srcIP = jp.getText();
                    break;
                case SRC_DEMAND:
                    src_demand = jp.getDoubleValue();
                    break;
                case DST:
                    dst = jp.getText();
                    break;
                case DST_IP:
                    dstIP = jp.getText();
                    break;
                case DST_DEMAND:
                    dst_demand = jp.getDoubleValue();
                    break;
                case BW:
                    bw = jp.getDoubleValue();
                    break;
                case LATENCY:
                    latency = jp.getDoubleValue();
                    break;
                case STATUS:
                    status = VirtualLink.Status.valueOf(jp.getText());
                    break;
                case ERROR:
                    error = jp.getText();
                default:
                    break;
            }
        }

        if (src != null && dst != null && bw >= 0 && latency >= 0 && status != null) {
            VirtualLink vl = new VirtualLink(src, srcIP, src_demand, dst, dstIP, dst_demand, bw,
                    latency == 0 ? Double.MAX_VALUE : latency, status);
            vl.setError(error);
            return vl;
        }
        else throw new IOException("wrong VL parameters");
    }
}
