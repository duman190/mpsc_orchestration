package net.floodlightcontroller.mpsc.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.floodlightcontroller.mpsc.discovery.model.ILink;
import net.floodlightcontroller.mpsc.discovery.model.INode;
import net.floodlightcontroller.mpsc.openflow.customservices.impl.VLHolder;
import net.floodlightcontroller.mpsc.web.model.VirtualLink;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import java.io.IOException;
import java.util.List;


/**
 * Created by chemo_000 on 4/23/2015.
 */
public class VLMeasurementResource extends ServerResource implements IJsonVL {

    @Post("json")
    public String measureVL(String json) {
        String user = getAttribute("user");
        if (user != null)
            try {
                //for each VL measure its available bandwidth assuming no traffic on the VL itself!
                //TODO think on multiple VLS estimation at once!!!
                List<VirtualLink> vls = jsonToVL(json);
                double[] avBw = new double[vls.size()];
                for(int i=0; i < vls.size(); i++){
                    double vlAvBw = Double.MAX_VALUE;
                    //TODO: go over all physical link segments belonging to each VL
                    List<INode<String>> partialPath = VLHolder.getInstance().getPartialPath(vls.get(i));
                    if (partialPath != null) {
                        for (int j = 0; j < partialPath.size() - 1; j++) {
                            ILink l = partialPath.get(j).getNeighbors().get(partialPath.get(j + 1));
                            double avBwInt = l.getBw();
                            vlAvBw = avBwInt < vlAvBw ? avBwInt : vlAvBw;
                        }
                        for (int j = partialPath.size() - 1; j > 0; j--) {
                            ILink l = partialPath.get(j).getNeighbors().get(partialPath.get(j - 1));
                            double avBwInt = l.getBw();
                            vlAvBw = avBwInt < vlAvBw ? avBwInt : vlAvBw;
                        }
                    }
                    avBw[i] = vlAvBw;
                }
                return arrayToJson(avBw);
            } catch (Exception ex) {
                ex.printStackTrace();
                return ("{\"ERROR\" : \"exception during VL measurement: " + ex.getMessage() + "\"}");
            }
        else
            return ("{\"ERROR\" : \"user=null\"}");
    }

    public static List<VirtualLink> jsonToVL(String json) throws IOException {
        System.out.println("Next JSON obtained: " + json);
        ObjectMapper mapper = new ObjectMapper();
        //deserialize JSON objects as a Set
        List<VirtualLink> vls = mapper.readValue(json,
                mapper.getTypeFactory().constructCollectionType(List.class, VirtualLink.class));
        return vls;
    }

    public static String arrayToJson(double[] d) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(d);

        return json;
    }

    public String getAttribute(String name) {
        Object value = this.getRequestAttributes().get(name);
        return value == null ? null : value.toString();
    }
}
