package net.floodlightcontroller.mpsc.web.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import net.floodlightcontroller.mpsc.web.IJsonVL;

import java.io.IOException;

/**
 * Created by chemo_000 on 4/29/2015.
 */
public class SwitchRuleDeserializer extends JsonDeserializer<SwitchRule> implements IJsonVL
{
    @Override
    public SwitchRule deserialize(JsonParser jp, DeserializationContext dC) throws IOException, JsonProcessingException
    {
        String src = null;
        String dst = null;
        double bw = -1.0;

        if (jp.getCurrentToken() != JsonToken.START_OBJECT)
        {
            throw new IOException("Expected START_OBJECT");
        }

        while (jp.nextToken() != JsonToken.END_OBJECT)
        {
            if (jp.getCurrentToken() != JsonToken.FIELD_NAME)
            {
                throw new IOException("Expected FIELD_NAME");
            }

            String n = jp.getCurrentName();
            jp.nextToken();
            if (jp.getText().equals(""))
            {
                continue;
            }

            switch (n)
            {
                case SRC:
                    src = jp.getText();
                    break;
                case DST:
                    dst = jp.getText();
                    break;
                case BW:
                    bw = jp.getDoubleValue();
                    break;
                default:
                    break;
            }
        }

        if (src != null && dst != null && bw >= 0)
            return new SwitchRule(src, dst, bw);
        else throw new IOException("wrong Switch Rule parameters");
    }
}
