package net.floodlightcontroller.mpsc.web.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by dmitriichemodanov on 9/19/18.
 */
@JsonSerialize(using = SwitchRuleSerializer.class)
@JsonDeserialize(using = SwitchRuleDeserializer.class)
public class SwitchRule {
    protected String src;
    protected String dst;
    protected double bw;

    public SwitchRule(String src, String dst, double bw) {
        this.src = src;
        this.dst = dst;
        this.bw = bw;
    }

    public String getSrc() {
        return this.src;
    }

    public String getDst() {
        return this.dst;
    }

    public double getBw() {
        return this.bw;
    }

    public String toString() {
        return "switch rule: allocate bw=" + bw + " from src=" + src + " to dst=" + dst;
    }
}
