package net.floodlightcontroller.mpsc.web.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import net.floodlightcontroller.mpsc.discovery.model.INode;

/**
 * Created by chemo_000 on 4/29/2015.
 */
@JsonSerialize(using = PhysicalLinkSerializer.class)
@JsonDeserialize(using = PhysicalLinkDeserializer.class)
public class PhysicalLink
{
    protected String src;
    protected INode.INodeType srcType;
    protected double srcCap;
    protected double srcStress;
    protected String dst;
    protected INode.INodeType dstType;
    protected double dstCap;
    protected double dstStress;
    protected double avBw = 0;
    protected double capacity = 0;
    protected double latency = 0;

    public PhysicalLink()
    {
    }

    public PhysicalLink(String src, INode.INodeType srcType, String dst,
                        INode.INodeType dstType, double avBw, double capacity, double latency) {
        this(src, srcType, 0, 0, dst, dstType, 0, 0, avBw, capacity, latency);
    }
    public PhysicalLink(String src, INode.INodeType srcType, double srcCap, double srcStress,
                        String dst, INode.INodeType dstType, double dstCap, double dstStress,
                        double avBw, double capacity, double latency) {
        this.src=src;
        this.srcType=srcType;
        this.srcCap = srcCap;
        this.srcStress = srcStress;
        this.dst=dst;
        this.dstType=dstType;
        this.dstCap=dstCap;
        this.dstStress=dstStress;
        this.avBw=avBw;
        this.capacity=capacity;
        this.latency =latency;
    }
}
