package mu.nexos.corectrl.web;

import mu.nexos.corectrl.MPSCConstants;
import mu.nexos.corectrl.model.ILink;
import mu.nexos.corectrl.model.INode;
import mu.nexos.corectrl.model.ITopology;
import mu.nexos.corectrl.model.TopoHolder;
import mu.nexos.corectrl.web.model.PhysicalLink;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import java.util.*;

/**
 * Created by chemo_000 on 4/29/2015.
 */
public class PNResource extends ServerResource implements IJsonVL
{
    @Get("json")
    public Set<PhysicalLink> retrieve()
    {
        System.out.println("[DEBUG] The client tries to retrieve the topo");
//        ILinkDiscoveryService linkService = (ILinkDiscoveryService) getContext().getAttributes()
//                .get(ILinkDiscoveryService.class.getCanonicalName());
        String user = getAttribute("user");
        if (user != null && user.equals("admin"))
        {
            System.out.println("[DEBUG] The client is admin and can do this");
            ITopology t = TopoHolder.instance.getTopo();
            t.updateTopoCache();

//            AllocatedBwEstimator bwEstimator = new AllocatedBwEstimator();
            //first collect info about switch-to-switch links
//            Set<PhysicalLink> pls = convertSwLinksToPhysicalLinks(getSwitchesLinks(linkService), bwEstimator);
            //then collect info about all known host links
//            pls.addAll(convertHostLinksToPhysicalLinks(bwEstimator));
            Set<PhysicalLink> pls = convertKnownLinksToPLs(t.getKnownLinks());

            return pls;
        } else return Collections.EMPTY_SET;
    }

    private Set<PhysicalLink> convertKnownLinksToPLs(Map<Pair<INode,INode>, ILink> links)
    {
        Set<PhysicalLink> pls = new HashSet<>();

        for (Map.Entry<Pair<INode,INode>, ILink> e : links.entrySet())
        {
            INode srcNode = e.getKey().getLeft();
            String src = srcNode.getID().toString();
            INode.INodeType srcType = srcNode.getType();

            INode dstNode = e.getKey().getRight();
            String dst = dstNode.getID().toString();
            INode.INodeType dstType = dstNode.getType();

            ILink link = e.getValue();

            Pair<String, String> linkKey = new ImmutablePair<>(src, dst);
            //Double Capacity = TopoConfigParser.getInstance().getLinkCapacity(linkKey);
            double capacity = link.getCapacity();//Capacity == null ? MPSCConstants.MAX_HOST_LINK_BW : Capacity;
            double avBw = link.getBw();
            double latency = link.getLatency();

            pls.add(new PhysicalLink(src, srcType, dst, dstType, avBw, capacity, latency));
        }

        return pls;
    }

    private Collection<? extends PhysicalLink> convertHostLinksToPhysicalLinks()
    {
        Set<PhysicalLink> pls = new LinkedHashSet<>();

//        for (Map.Entry<Pair<IPv4Address, DatapathId>, Short> entry : knownHosts.entrySet())
//        {
            String src = "";//entry.getKey().getLeft().toString();
            INode.INodeType srcType = INode.INodeType.REGULAR;//INode.INodeType.HOST;
            String dst = "";//entry.getKey().getRight().toString();
            INode.INodeType dstType = INode.INodeType.REGULAR;//INode.INodeType.SWITCH;
            Pair<String, String> linkKey = new ImmutablePair<>(src, dst);
            Double Capacity = 0.0;//TopoConfigParser.getInstance().getLinkCapacity(linkKey);
            double capacity = Capacity == null ? MPSCConstants.MAX_HOST_LINK_BW : Capacity;
            double avBw = capacity - 0;//bwEstimator.estimateAllocatedBw(entry.getKey().getRight(), entry.getValue(), flowPusher);
            Double Cost = 0.0;//TopoConfigParser.getInstance().getLinkLatency(linkKey);
            double cost = Cost == null ? 0.0 : Cost;

            pls.add(new PhysicalLink(src, srcType, dst, dstType, avBw, capacity, cost));
//        }

        return pls;
    }

    private Set<PhysicalLink> convertSwLinksToPhysicalLinks()
    {
        Set<PhysicalLink> pls = new LinkedHashSet<>();

//
//        for (Link l : switchesLinks)
//        {
//            String src = l.getSrc().toString();
//            INode.INodeType srcType = INode.INodeType.SWITCH;
//            String dst = l.getDst().toString();
//            INode.INodeType dstType = INode.INodeType.SWITCH;
//            Pair<String, String> linkKey = new ImmutablePair<>(src, dst);
//            Double Capacity = TopoConfigParser.getInstance().getLinkCapacity(linkKey);
//            double capacity = Capacity == null ? MPSCConstants.MAX_SWITCH_LINK_BW : Capacity;
//            double avBw = capacity - bwEstimator.estimateAllocatedBw(l.getSrc(), l.getSrcPort().getShortPortNumber(), flowPusher);
//            Double Cost = TopoConfigParser.getInstance().getLinkLatency(linkKey);
//            double cost = Cost == null ? 0.0 : Cost;
//            pls.add(new PhysicalLink(src, srcType, dst, dstType, avBw, capacity, cost));
//        }

        return pls;
    }

    /*
    private Set<Link> getSwitchesLinks(ILinkDiscoveryService linkService)
    {
        Set<Link> links = new LinkedHashSet<>();
        Set<Pair<DatapathId, DatapathId>> knownSwitches = new LinkedHashSet<>();

        for (Map.Entry<DatapathId, Set<Link>> entry : linkService.getSwitchLinks().entrySet())
            for (Link l : entry.getValue())
            {
                Pair<DatapathId, DatapathId> linkKey1 = new ImmutablePair<>(l.getSrc(), l.getDst());
                Pair<DatapathId, DatapathId> linkKey2 = new ImmutablePair<>(l.getDst(), l.getSrc());
                if (!knownSwitches.contains(linkKey1) && !knownSwitches.contains(linkKey2))
                {
                    links.add(l);

                    knownSwitches.add(linkKey1);
//                    knownSwitches.add(linkKey2);
                }
            }
        return links;
    }
*/
    public String getAttribute(String name) {
        Object value = this.getRequestAttributes().get(name);
        return value == null?null:value.toString();
    }
}
