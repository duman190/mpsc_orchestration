package mu.nexos.corectrl.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mu.nexos.corectrl.main.VLHolder;
import mu.nexos.corectrl.mapping.optimization.MetapathOpt;
import mu.nexos.corectrl.model.ILink;
import mu.nexos.corectrl.model.ITopology;
import mu.nexos.corectrl.model.TopoHolder;
import mu.nexos.corectrl.distrctrl.DistrCtrlHolder;
import mu.nexos.corectrl.distrctrl.DistrCtrlKey;
import mu.nexos.corectrl.web.model.SwitchRule;
import mu.nexos.corectrl.web.model.VirtualLink;
import org.apache.commons.lang3.tuple.Pair;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
import mu.nexos.corectrl.model.INode;

import java.io.IOException;
import java.util.*;

import static mu.nexos.corectrl.web.model.VirtualLink.Status.ALLOCATED;
import static mu.nexos.corectrl.web.model.VirtualLink.Status.FAILED;

/**
 * Created by chemo_000 on 4/23/2015.
 */
public class SCResource extends ServerResource implements IJsonVL {
    @Get("json")
    public List<VirtualLink> retrieve() {
        String user = getAttribute("user");
        List<VirtualLink> res = null;

        if (user != null)
            res = VLHolder.getInstance().getUserVL(user);

        //TODO: check status from distr cntrls

        if (res != null)
            return res;
        else
            return Collections.EMPTY_LIST;
    }

    @Post("json")
    public String allocateSC(String json) {
        String user = getAttribute("user");
        if (user != null)
            try {
                List<VirtualLink> vls = jsonToVL(json);
                //TODO: verify SC request
                //Compose SC
                Pair<Map<String, INode>, Map<VirtualLink, List<INode<String>>>> scMappings;
                String compositionEx = "";
                ITopology t = TopoHolder.instance.getTopo();
                try {
                    //TODO: think on smart cache update
                    t.updateTopoCache();
                    System.out.println("[DEBUG-MPSC] Updated topology:");
                    for (INode n : t.getKnownNodes())
                        System.out.println("[DEBUG-MPSC] Node id=" + n.getID()
                                + " cap=" + n.getCapacity()
                                + " stress=" + n.getStress()
                                + " neighbors=" + n.getNeighbors().keySet());

                    for (Pair<INode, INode> pair : t.getKnownLinks().keySet()) {
                        ILink l = t.getKnownLinks().get(pair);
                        System.out.println("[DEBUG-MPSC] Link src=" + pair.getLeft().getID()
                                + " dst=" + pair.getRight().getID()
                                + " cap=" + l.getCapacity() + " bw=" + l.getBw()
                                + " lat=" + l.getLatency());
                    }

                    for(DistrCtrlKey key : DistrCtrlHolder.instance.getSubscribers())
                        for(INode n : t.getSwitchDependentNodes(key))
                        System.out.println("[DEBUG-MPSC] Distr Cntrl Key=" + key + ": Node=" + n);

                    scMappings = new MetapathOpt().mapServiceChain(vls,
                            t, t.getKnownNodes().size());

                    Map<String, INode> vnMappings = scMappings.getLeft();
                    //TODO: add proper logs here
                    System.out.println("[DEBUG-MPSC] Resulting VNode mapping is the following:");
                    for (String s : vnMappings.keySet())
                        System.out.println("[DEBUG-MPSC] VNode id=" + s + " to pnode id=" + vnMappings.get(s).getID());

                    Map<VirtualLink, List<INode<String>>> vlMappings = scMappings.getRight();
                    System.out.println("[DEBUG-MPSC] Resulting VLink mapping is the following:");
                    for (VirtualLink l : vlMappings.keySet())
                        System.out.println("[DEBUG-MPSC] VLinks src=" + l.getSrc() + " dst=" + l.getDst()
                                + " to path=" + vlMappings.get(l));
                } catch (Exception ex) {
                    compositionEx = ex.getMessage();
                    ex.printStackTrace();
                    scMappings = null;
                }
                if (scMappings != null) {
                    Set<INode> allocatedPNs = new HashSet<>();
                    //assign bare metal server IP to each service if no containers are used
                    //TODO: allow distr cntrl to assign a docker container IP instead allocated at found server
                    for (VirtualLink vl : vls) {
                        //retrieve corresponding physical nodes
                        INode pSrc = scMappings.getLeft().get(vl.getSrc());
                        INode pDst = scMappings.getLeft().get(vl.getDst());
                        //assign IP addresses
                        vl.setSrcIP(pSrc.getID().toString());
                        vl.setDstIP(pDst.getID().toString());
                        /*
                        //allocate physical node resources
                        if (!allocatedPNs.contains(pSrc)) {
                            pSrc.setStress(pSrc.getStress() + vl.getSrcCap());
                            allocatedPNs.add(pSrc);
                        }
                        if (!allocatedPNs.contains(pDst)) {
                            pDst.setStress(pDst.getStress() + vl.getDstCap());
                            allocatedPNs.add(pDst);
                        }*/
                    }

                    Map<DistrCtrlKey, Map<VirtualLink, List<SwitchRule>>> switchVLMappings = new HashMap<>();
                    //group corresponding SC flows per each cntrl
                    //TODO: validate switch rules composition
                    for (DistrCtrlKey key : DistrCtrlHolder.instance.getSubscribers()) {
                        Set<INode> switchDependentNodes = t.getSwitchDependentNodes(key);
                        Map<VirtualLink, List<SwitchRule>> vlMappings = new LinkedHashMap<>();
                        for (VirtualLink vl : vls) {
                            List<SwitchRule> newSwitchRules = new ArrayList<>(scMappings.getRight().get(vl).size() - 1);
                            List<INode<String>> pRoute = scMappings.getRight().get(vl);
                            for (int i = 0; i < pRoute.size() - 1; i++)
                                if (switchDependentNodes.contains(pRoute.get(i))
                                        && switchDependentNodes.contains(pRoute.get(i + 1)))
                                    newSwitchRules.add(new SwitchRule(pRoute.get(i).getID(),
                                            pRoute.get(i + 1).getID(), vl.getBw()));
                            vlMappings.put(vl, newSwitchRules);
                        }
                        switchVLMappings.put(key, vlMappings);
                    }
                    //TODO: recursive and parallel calls to cntrls
                    //send allocation requests to corresponding controllers
                    Map<VirtualLink, Set<DistrCtrlKey>> vlToCntrls = new HashMap<>();
                    Map<VirtualLink, String> vlToErrorMsg = new HashMap<>();
                    for (DistrCtrlKey key : switchVLMappings.keySet()) {
                        List<VirtualLink> vls1 = AllocatePNResource.allocate(key, switchVLMappings.get(key));
                        for (VirtualLink vl : vls1) {
                            if (!vlToCntrls.containsKey(vl))
                                vlToCntrls.put(vl, new HashSet<>());
                            if (vl.getStatus().equals(VirtualLink.Status.ALLOCATED))
                                vlToCntrls.get(vl).add(key);
                            else if (vl.getStatus().equals(FAILED))
                                vlToErrorMsg.put(vl, vl.getError());
                        }
                    }
                    System.out.println("[DEBUG-MPSC] Resulting Distr Cntrls replies:");
                    System.out.println("[DEBUG-MPSC] VL to Cntrl Map:" + vlToCntrls);
                    System.out.println("[DEBUG-MPSC] VL to Error Map:" + vlToErrorMsg);
                    Boolean removeResources = false;
                    for (VirtualLink vl : vls)
                        if (vlToCntrls.get(vl) != null &&
                                vlToCntrls.get(vl).size() == switchVLMappings.keySet().size())
                            vl.setStatus(ALLOCATED);
                        else {
                            vl.setStatus(FAILED);
                            vl.setError("Error msg=" + vlToErrorMsg.get(vl));
                            removeResources = true;
                        }

                    //remove corresponding resources
                    if(removeResources)
                        removeSwitchResources(switchVLMappings);
                } else //sc composition has failed! Report back the result!
                {
                    for (VirtualLink vl : vls) {
                        vl.setStatus(FAILED);
                        vl.setError("SC composition exception!");
                    }
                }
                VLHolder.getInstance().addUserVL(user, vls);
                return vlToJson(vls);
            } catch (Exception ex) {
                ex.printStackTrace();
                return ("{\"ERROR\" : \"exception during service chain composition: " + ex.getMessage() + "\"}");
            }
        else
            return ("{\"ERROR\" : \"user=null\"}");
    }

    public static List<VirtualLink> jsonToVL(String json) throws IOException {
        System.out.println("Next JSON obtained: " + json);
        ObjectMapper mapper = new ObjectMapper();
        //deserialize JSON objects as a Set
        List<VirtualLink> vls = mapper.readValue(json,
                mapper.getTypeFactory().constructCollectionType(List.class, VirtualLink.class));
        return vls;
    }

    public static String vlToJson(List<VirtualLink> vls) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(vls);

        return json;
    }

    public String getAttribute(String name) {
        Object value = this.getRequestAttributes().get(name);
        return value == null ? null : value.toString();
    }

    public void removeSwitchResources(Map<DistrCtrlKey, Map<VirtualLink, List<SwitchRule>>> switchVLMappings)
    {
        Map<VirtualLink, String> vlToErrorMsg = new HashMap<>();
        for (DistrCtrlKey key : switchVLMappings.keySet())
            RemovePNResource.remove(key, switchVLMappings.get(key));
    }
}
