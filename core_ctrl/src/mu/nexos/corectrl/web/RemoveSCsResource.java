package mu.nexos.corectrl.web;

import mu.nexos.corectrl.distrctrl.DistrCtrlHolder;
import mu.nexos.corectrl.distrctrl.DistrCtrlKey;
import mu.nexos.corectrl.main.VLHolder;
import org.restlet.data.MediaType;
import org.restlet.resource.ClientResource;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

/**
 * Created by chemo_000 on 4/23/2015.
 */
public class RemoveSCsResource extends ServerResource {
    @Get
    public void retrieve() {
        String user = getAttribute("user");
        if (user != null) {
            //remove all SC flows
            VLHolder.getInstance().removeAllVLs();
            for (DistrCtrlKey key : DistrCtrlHolder.instance.getSubscribers()) {
                //the path should be the same for all distributed SDN cntrls
                String URI = "http://" + key.getIP() + ":" + key.getPort() + "/wm/mpsc/Remove_all_SCs/" + user;
                System.out.println("[DEBUG-MPSC-REMOVE-SCs] Switch URI=" + URI);
                ClientResource resource = new ClientResource(URI);
                try {
                    resource.get(); //
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public String getAttribute(String name) {
        Object value = this.getRequestAttributes().get(name);
        return value == null ? null : value.toString();
    }
}
