package mu.nexos.corectrl.web.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import mu.nexos.corectrl.model.INode;
import mu.nexos.corectrl.web.IJsonVL;

import java.io.IOException;

/**
 * Created by chemo_000 on 4/29/2015.
 */
public class INodeSerializer extends JsonSerializer<INode<String>> implements IJsonVL
{
    @Override
    public void serialize(INode<String> node, JsonGenerator jgen, SerializerProvider arg) throws IOException, JsonProcessingException
    {
        jgen.writeStartObject();
        jgen.writeStringField(SRC, node.getID());
        jgen.writeStringField(SRC_TYPE, node.getType().toString());
        jgen.writeNumberField(SRC_CAP, node.getCapacity());
        jgen.writeNumberField(SRC_STRESS, node.getStress());
        jgen.writeEndObject();
    }
}
