package mu.nexos.corectrl.web.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import mu.nexos.corectrl.model.INode;
import mu.nexos.corectrl.model.impl.MetaNode;
import mu.nexos.corectrl.model.impl.NodeBase;
import mu.nexos.corectrl.web.IJsonVL;

import java.io.IOException;

/**
 * Created by chemo_000 on 4/29/2015.
 */
public class INodeDeserializer extends JsonDeserializer<INode<String>> implements IJsonVL
{
    @Override
    public INode<String> deserialize(JsonParser jp, DeserializationContext dC) throws IOException, JsonProcessingException
    {
        String src = null;
        INode.INodeType srcType = null;
        double srcCap = -1.0;
        double srcStress = -1.0;


        if (jp.getCurrentToken() != JsonToken.START_OBJECT)
        {
            throw new IOException("Expected START_OBJECT");
        }

        while (jp.nextToken() != JsonToken.END_OBJECT)
        {
            if (jp.getCurrentToken() != JsonToken.FIELD_NAME)
            {
                throw new IOException("Expected FIELD_NAME");
            }

            String n = jp.getCurrentName();
            jp.nextToken();
            if (jp.getText().equals(""))
            {
                continue;
            }

            switch (n)
            {
                case SRC:
                    src = jp.getText();
                    break;
                case SRC_TYPE:
                    srcType = INode.INodeType.valueOf(jp.getText());
                    break;
                case SRC_CAP:
                    srcCap = jp.getDoubleValue();
                    break;
                case SRC_STRESS:
                    srcStress = jp.getDoubleValue();
                    break;
                default:
                    break;
            }
        }

        if (src != null && srcType!=null)
            switch (srcType)
            {
                case HOST:
                case SWITCH:
                case REGULAR:
                    return new NodeBase<>(src, srcType, srcCap, srcStress);
                case META_NODE:
                    return new MetaNode<>(src, srcCap);
                default:
                    break;
            }
        throw new IOException("wrong INode parameters");
    }
}
