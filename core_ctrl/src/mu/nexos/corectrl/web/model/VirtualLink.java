package mu.nexos.corectrl.web.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chemo_000 on 4/23/2015.
 */
@JsonSerialize(using = VirtualLinkSerializer.class)
@JsonDeserialize(using = VirtualLinkDeserializer.class)
public class VirtualLink {
    protected String src;
    protected String srcIP;
    protected double src_cap = 0;
    protected String dst;
    protected String dstIP;
    protected double dst_cap = 0;
    protected double bw = 0;
    protected double latency = 0;
    protected Status status = Status.FAILED;
    protected String error = "";

    protected List<MPath> mpCandidates;

    public enum Status {
        ALLOCATED, FAILED, PENDING
    }

    public VirtualLink() {
    }

    public VirtualLink(String src, double src_cap, String dst, double dst_cap, double bw, double latency, Status status) {
        this.src = src;
        this.srcIP = "-";
        this.src_cap = src_cap;
        this.dst = dst;
        this.dst_cap = dst_cap;
        this.dstIP = "-";
        this.bw = bw;
        this.latency = latency;
        this.status = status;
        this.mpCandidates = new ArrayList<>();
    }

    public VirtualLink(String src, String srcIP, double src_cap, String dst, String dstIP, double dst_cap, double bw, double latency, Status status) {
        this(src, src_cap, dst, dst_cap, bw, latency, status);
        this.srcIP = srcIP;
        this.dstIP = dstIP;
    }

    public String getSrc() {
        return src;
    }

    public String getDst() {
        return dst;
    }

    public String getSrcIP() {
        return srcIP;
    }

    public String getDstIP() {
        return dstIP;
    }

    public void setSrcIP(String srcIP) {
        this.srcIP = srcIP;
    }

    public void setDstIP(String dstIP) {
        this.dstIP = dstIP;
    }

    public double getSrcCap() {
        return this.src_cap;
    }

    public double getDstCap() {
        return this.dst_cap;
    }

    public double getBw() {
        return bw;
    }

    public double getLatency() {
        return latency;
    }

    public Status getStatus() {
        return status;
    }

    public String getError() {
        return error;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String toString() {
        return "[" + src + "->" + dst + ": bw=" + bw + ", lat=" + latency + "]";
    }

    public int hashCode()
    {
        return toString().hashCode();
    }

    public boolean equals(Object o) {
        if (o != null && o instanceof VirtualLink) {
            VirtualLink vl = (VirtualLink) o;
            if (this.src.equals(vl.src) && this.dst.equals(vl.getDst())
                    && this.bw == vl.bw && this.latency == vl.latency)
                return true;
        }
        return false;
    }

    public List<MPath> getMpCandidates()
    {
        return this.mpCandidates;
    }

    public void setMpCandidates(List<MPath> mpCandidates)
    {
        this.mpCandidates = mpCandidates;
    }
}
