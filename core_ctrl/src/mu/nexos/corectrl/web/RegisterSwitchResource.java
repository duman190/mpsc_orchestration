package mu.nexos.corectrl.web;

import mu.nexos.corectrl.distrctrl.DistrCtrlHolder;
import mu.nexos.corectrl.distrctrl.DistrCtrlKey;
import org.restlet.Request;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

/**
 * Created by dmitriichemodanov on 9/14/18.
 */
public class RegisterSwitchResource extends ServerResource implements IJsonVL {
    @Get("json")
    public void register() {
        String ip = Request.getCurrent().getClientInfo().getAddress();//.getForwardedAddresses();
        int port = 8080;//Request.getCurrent().getClientInfo().getPort();

        DistrCtrlHolder.instance.addSwitchSubscriber(new DistrCtrlKey(ip, port));
    }
}