package mu.nexos.corectrl.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mu.nexos.corectrl.model.ILink;
import mu.nexos.corectrl.model.INode;
import mu.nexos.corectrl.model.impl.LinkBase;
import mu.nexos.corectrl.model.impl.NodeBase;
import mu.nexos.corectrl.distrctrl.DistrCtrlKey;
import mu.nexos.corectrl.web.model.PhysicalLink;
import mu.nexos.corectrl.web.model.VirtualLink;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ClientResource;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by dmitriichemodanov on 9/17/18.
 */
public class SwitchClientResource {
    public static void retrieveSwitchTopo(DistrCtrlKey key, Set<INode> nodes, Map<Pair<INode, INode>, ILink> links) {

        //the path should be the same for all distributed SDN cntrls
        String URI = "http://" + key.getIP() + ":" + key.getPort() + "/wm/mpsc/PN/admin";
        System.out.println("[DEBUG] Switch URI=" + URI);
        ClientResource resource = new ClientResource(URI);
        try {
            String str = resource.get(MediaType.APPLICATION_JSON).getText(); //
            ObjectMapper mapper = new ObjectMapper();
            //deserialize JSON objects as a Set
            List<PhysicalLink> pls = mapper.readValue(str,
                    mapper.getTypeFactory().constructCollectionType(List.class, PhysicalLink.class));
            for (PhysicalLink pl : pls) {
                //TODO: check how to handle uncontrollable switches
                INode<String> src = new NodeBase<>(pl.getSrc(), pl.getSrcType(), pl.getSrcCap(), pl.getSrcStress());
                INode<String> dst = new NodeBase<>(pl.getDst(), pl.getDstType(), pl.getDstCap(), pl.getDstStress());
                ILink link = new LinkBase(pl.getAvBw(), pl.getCapacity(), pl.getLatency());
                nodes.add(src);
                nodes.add(dst);
                links.put(new ImmutablePair<>(src, dst), link);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static double[] measureVLs(DistrCtrlKey key, List<VirtualLink> vls) {
        //call to distr controller
        //the path should be the same for all distributed SDN cntrls
        double[] avBw = new double[vls.size()];
        String URI = "http://" + key.getIP() + ":" + key.getPort() + "/wm/mpsc/VL/measure/admin";
        System.out.println("[DEBUG] Switch URI=" + URI);
        ClientResource resource = new ClientResource(URI);
        try {
            String requestJSON = vlToJson(vls);
            System.out.println("[DEBUG-MPSC-Measurement] Json request=" + requestJSON);
            Representation req = new StringRepresentation(requestJSON, MediaType.APPLICATION_JSON);
            String avBwString = resource.post(req).getText();
            System.out.println("[DEBUG-MPSC-Measurement] Json reply=" + avBwString);
            avBw = jsonToArray(avBwString);
        } catch (Exception ex) {
            System.out.println("[MPSC-Measurement] Error occured while contacting measurement engine of cntrl="
                    + key + "; error msg=" + ex.getMessage());
            ex.printStackTrace();
        }
        return avBw;
    }

    public static double[] jsonToArray(String json) throws IOException {
        System.out.println("Next JSON obtained: " + json);
        ObjectMapper mapper = new ObjectMapper();
        //deserialize JSON objects as a Set
        Double[] d = mapper.readValue(json, Double[].class);
        return ArrayUtils.toPrimitive(d);
    }

    public static String vlToJson(List<VirtualLink> vls) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(vls);

        return json;
    }
}
