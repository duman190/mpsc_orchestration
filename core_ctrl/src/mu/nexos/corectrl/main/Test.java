package mu.nexos.corectrl.main;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mu.nexos.corectrl.web.model.SwitchRule;
import mu.nexos.corectrl.web.model.VLRules;
import mu.nexos.corectrl.web.model.VirtualLink;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by dmitriichemodanov on 4/8/18.
 */
public class Test {
    public static void main(String[] args) {
        try {
            String json = "[{\"source\":\"00:00:00:00:00:00:00:02\",\"destination\":\"00:00:00:00:00:00:00:01\",\"bandwidth\":50.0},{\"source\":\"00:00:00:00:00:00:00:01\",\"destination\":\"10.0.0.1\",\"bandwidth\":50.0}]";

            SwitchRule swR1 = new SwitchRule("00:00:00:00:00:00:00:02", "00:00:00:00:00:00:00:01", 50);
            SwitchRule swR2 = new SwitchRule("00:00:00:00:00:00:00:01", "10.0.0.1", 50);
            List<SwitchRule> switchRules = new ArrayList<>(2);
            switchRules.add(swR1);
            switchRules.add(swR2);

            VLRules vlR = new VLRules(new VirtualLink("A", "10.0.0.1", 2, "B", "10.0.0.2", 3, 50, 10, VirtualLink.Status.PENDING),
                    switchRuleToJson(switchRules));

            String response = vlRulesToJson(Collections.singletonList(vlR));
            System.out.println("VL Rule as JSON=" + response);
            ObjectMapper mapper = new ObjectMapper();
            List<VLRules> vlR1 = mapper.readValue(response,
                    mapper.getTypeFactory().constructCollectionType(List.class, VLRules.class));
            System.out.println("VL Rule w VL=" + vlR1.get(0).getVL() + " and switch rules=" + vlR1.get(0).getSwitchRules() + "; their JSON=" + vlR1.get(0).getSwitchRulesAsJSON());



            List<SwitchRule> rules = mapper.readValue(vlR1.get(0).getSwitchRulesAsJSON(),
                    mapper.getTypeFactory().constructCollectionType(List.class, SwitchRule.class));
            System.out.println("Switch rules=" + rules);


            String vl = "[{\"source\":\"A\",\"srcIP\":\"10.0.0.2\",\"src_demand\":2.0,\"destination\":\"B\",\"dstIP\":\"10.0.0.1\",\"dst_demand\":3.0,\"bandwidth\":50.0,\"latency\":10.0,\"status\":\"FAILED\",\"error\":\"[MPSC-Allocation] Wrong switch rule: either src=00:00:00:00:00:00:00:01 or dst=10.0.0.1  or both are unknown!\"}]";
            List<VirtualLink> vls = jsonToVL(vl);
            System.out.println("VL=" + vls.get(0) + " error msg=" + vls.get(0).getError());

        } catch (IOException ex) {
            //TODO log exception
            ex.printStackTrace();
        }
    }

    private static String switchRuleToJson(List<SwitchRule> switchRules) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(switchRules);

        return json;
    }


    private static String vlRulesToJson(List<VLRules> vlRulesList) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(vlRulesList);

        return json;
    }

    public static List<VirtualLink> jsonToVL(String json) throws IOException {
        System.out.println("Next JSON obtained: " + json);
        ObjectMapper mapper = new ObjectMapper();
        //deserialize JSON objects as a Set
        List<VirtualLink> vls = mapper.readValue(json,
                mapper.getTypeFactory().constructCollectionType(List.class, VirtualLink.class));
        return vls;
    }
}
