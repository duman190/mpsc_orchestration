package mu.nexos.corectrl.main;

import mu.nexos.corectrl.CoreServerConstants;
import mu.nexos.corectrl.web.CoreRestApp;
import org.restlet.Component;
import org.restlet.data.Protocol;

public class Main implements CoreServerConstants {
    public static void main(String[] args) throws Exception {
            final Component component = new Component();
            component.getServers().add(Protocol.HTTP, SERVER_PORT);
            component.getClients().add(Protocol.CLAP);
            component.getClients().add(Protocol.HTTP);
//            component.getClients().add(Protocol.HTTPS);
//            component.getClients().add(Protocol.FILE);
            CoreRestApp app = new CoreRestApp();
            component.getDefaultHost().attach(app);
            component.start();

//        Form form = new Form();
//        form.add("x", "foo");
//        form.add("y", "bar");
//        ClientResource resource = new ClientResource("http://[IP]:8080/someresource");
//        Response response = null;//resource.post(form.getWebRepresentation());
//        System.out.println(response.getEntity().getText());
//        if (response.getStatus().isSuccess()) {
//     }
    }
}
