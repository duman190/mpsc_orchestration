package mu.nexos.corectrl.main;

import com.fasterxml.jackson.databind.ObjectMapper;
import mu.nexos.corectrl.web.model.PhysicalLink;
import org.restlet.data.MediaType;
import org.restlet.resource.ClientResource;

import java.util.Arrays;
import java.util.List;

/**
 * Created by dmitriichemodanov on 4/8/18.
 */
public class TestREST {
    public static void main(String[] args) {
        ClientResource resource = new ClientResource("http://127.0.0.1:8080/wm/mpsc/PN/admin");
        try {
            String str = resource.get(MediaType.APPLICATION_JSON).getText();
            System.out.println("Response:" + str);
            ObjectMapper mapper = new ObjectMapper();
            //deserialize JSON objects as a Set
            List<PhysicalLink> pls = mapper.readValue(str,
                    mapper.getTypeFactory().constructCollectionType(List.class, PhysicalLink.class));
            System.out.println("Parsed response:" + pls);
        }catch (Exception ex)
        {
            ex.printStackTrace();
        }

        //System.out.println();
    }

    private static double compute_total_exec_time(int[] acts, double[] exec_times)
    {
        double exec_time = 0;
        for(int i=0; i< acts.length; i++)
            if(acts[i] == 1)
                exec_time += exec_times[i];
        return exec_time;
    }

    private static double compute_risk(int[] acts, double[] risks)
    {
        double log_risk = 0;
        for(int i=0; i< acts.length; i++)
            if(acts[i] == 1)
                log_risk += Math.log(risks[i]);
        return Math.exp(log_risk);
    }
}
