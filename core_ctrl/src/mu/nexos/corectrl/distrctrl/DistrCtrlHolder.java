package mu.nexos.corectrl.distrctrl;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by dmitriichemodanov on 9/14/18.
 */
public class DistrCtrlHolder {
    public static DistrCtrlHolder instance = new DistrCtrlHolder();

    private Set<DistrCtrlKey> subscribedSwitches;

    private DistrCtrlHolder()
    {
        subscribedSwitches = new HashSet<>();
    }

    public Set<DistrCtrlKey> getSubscribers()
    {
        return this.subscribedSwitches;
    }

    public boolean isSwitchAlive(DistrCtrlKey key)
    {
        if (subscribedSwitches.contains(key))
        {
           //TODO implenet a switch alive check with REST API
        }

        return false;
    }

    public void addSwitchSubscriber (DistrCtrlKey key)
    {
        subscribedSwitches.add(key);
    }
}
