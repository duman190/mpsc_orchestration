package mu.nexos.corectrl.distrctrl;

import mu.nexos.corectrl.web.model.VirtualLink;

/**
 * Created by dmitriichemodanov on 9/14/18.
 */
public class DistrCtrlKey {
    private String ip;
    private int port;

    public DistrCtrlKey(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public String getIP() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public String toString() {
        return ip + ":" + port;
    }

    public int hashCode() {
        return toString().hashCode();
    }

    public boolean equals(Object o) {
        if (o != null && o instanceof DistrCtrlKey) {
            DistrCtrlKey key = (DistrCtrlKey) o;
            if (this.ip.equals(key.ip) && this.port == key.port)
                return true;
        }
        return false;
    }
}
