package mu.nexos.corectrl.mapping.optimization;

import ilog.concert.*;
import ilog.cplex.IloCplex;

import mu.nexos.corectrl.mapping.routing.GeneralKBestNM;
import mu.nexos.corectrl.model.ILink;
import mu.nexos.corectrl.model.INode;
import mu.nexos.corectrl.model.ITopology;
import mu.nexos.corectrl.model.impl.MetaLink;
import mu.nexos.corectrl.model.impl.MetaNode;
import mu.nexos.corectrl.web.model.MPath;
import mu.nexos.corectrl.web.model.VirtualLink;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;

/* TODO: implement risks later
import static Util.Stat.calculateDemand;
import static Util.Util.nodeIsFeasible;
import static Util.Util.pnSatisfiesRisk;
*/

/**
 * General assignment problem is solved for optimal SC assignment.
 * First, we find k Constrained Shortest Metapaths. We then use them to construct linear assignment problem
 */
public class MetapathOpt {
    private GeneralKBestNM kBestNM;
    private Map<String, Set<INode<String>>> vNodeCandidates;
    private Map<VirtualLink, List<Pair<List<INode<String>>, Double>>> vLinkCandidates;
    private Map<String, Integer> vNodeTimes;

    public MetapathOpt() {
        kBestNM = new GeneralKBestNM();
        vNodeCandidates = new LinkedHashMap<>();
        vLinkCandidates = new LinkedHashMap<>();
        vNodeTimes = new LinkedHashMap<>();
    }

    public Pair<Map<String, INode>, Map<VirtualLink, List<INode<String>>>> mapServiceChain(List<VirtualLink> vls, ITopology g, int k) {
        vNodeCandidates.clear();
        vLinkCandidates.clear();
        vNodeTimes.clear();

        Map<String, INode> vNodeToPNode = new LinkedHashMap<>();
        Map<VirtualLink, List<INode<String>>> vLinkToPath = new LinkedHashMap<>();
        //find all metapaths before constructing the problem
        findMetapaths(vls, g, k);
        Map<ILink, Set<Pair<Integer, Integer>>> peToMPs = findPedgeCapacityVars(vls);
        Map<INode, Set<Pair<Integer, Integer>>> pnToMPs = findPnodeCapacityVars(vls);
        //solve master problem to get the optimal solution
        solveMaster(vls, g, vNodeToPNode, vLinkToPath, peToMPs, pnToMPs);


        //return resource mappings
        return new ImmutablePair<>(vNodeToPNode, vLinkToPath);
    }

    private void solveMaster(List<VirtualLink> vls, ITopology g, Map<String, INode> vNodeToPNode,
                             Map<VirtualLink, List<INode<String>>> vLinkToPath,
                             Map<ILink, Set<Pair<Integer, Integer>>> peToMPs,
                             Map<INode, Set<Pair<Integer, Integer>>> pnToMPs) {
        try {
            IloCplex cplex = new IloCplex();
            //double[][] initSol = new double[vls.size()][]; //var to store result
            IloNumVar[][] var = new IloNumVar[vls.size()][]; //we have VE types of vars
            IloRange[][] rng = new IloRange[vls.size() + 2][]; //we have VN + 1 types of constraints
            //construct LP and solve for near optimal (due to k constraint) assignment
            constructMasterProblem(cplex, var, rng, vls, peToMPs, pnToMPs);
            cplex.setOut(null);
            cplex.setParam(IloCplex.Param.TimeLimit, 120); //todo parametrize
            if (cplex.solve()) // if cplex found LP solution continue, otherwise reject VNR
            {
                //map vedges
                for (int ve = 0; ve < vls.size(); ve++) {
                    double[] f = cplex.getValues(var[ve]);
                    //initSol[ve] = f;
                    for (int j = 0; j < f.length; j++) {
                        if (f[j] > 0)
                            mapMetapathResources(vLinkCandidates.get(vls.get(ve)).get(j).getKey(),
                                    vNodeToPNode, vLinkToPath, vls.get(ve));
//                        System.out.print(var[vedge.getID()][j] + "=" + f[j] + ", ");
                    }
//                    System.out.print("\n");
                }
            } else
                throw new RuntimeException("[MSC] Mapping of the VNR has failed after linear assignment solution.");
            cplex.end();
        } catch (IloException e) {
            throw new RuntimeException("Concert exception caught '" + e + "' caught");
        }
    }


    private boolean nodeIsFeasible(INode pn, double cap) {
        return pn.getCapacity() - pn.getStress() >= cap;
    }

    private void initVNodeTimes(List<VirtualLink> vls) {
        vNodeTimes.clear();

        for (String vn : vNodeCandidates.keySet()) {
            int times = 0;
            for (VirtualLink vl : vls)
                if (vn.equals(vl.getSrc()))
                    times++;
                else if (vn.equals(vl.getDst()))
                    times++;

            vNodeTimes.put(vn, times);
        }
    }

    private void findMetapaths(List<VirtualLink> vls, ITopology g, int k) {
        //first find potential service mappings
        for (VirtualLink vl : vls) {
            if (!vNodeCandidates.containsKey(vl.getSrc()))
                vNodeCandidates.put(vl.getSrc(), new LinkedHashSet<>());

            if (!vNodeCandidates.containsKey(vl.getDst()))
                vNodeCandidates.put(vl.getDst(), new LinkedHashSet<>());

            for (INode pnode : g.getKnownNodes()) {
                if (nodeIsFeasible(pnode, vl.getSrcCap()))
                    vNodeCandidates.get(vl.getSrc()).add(pnode);

                if (nodeIsFeasible(pnode, vl.getDstCap()))
                    vNodeCandidates.get(vl.getDst()).add(pnode);
            }
        }

        initVNodeTimes(vls);

        //then find k Constrained Shortest Metapaths for each vlink
        for (VirtualLink vl : vls) {
            if (!vLinkCandidates.containsKey(vl))
                vLinkCandidates.put(vl, new ArrayList<>());

            INode<String> mSrc = convertToMetanode(vl.getSrc(), vl.getSrcCap());
            INode<String> mDst = convertToMetanode(vl.getDst(), vl.getDstCap());

            for (Map.Entry<Pair<INode, INode>, ILink> pe_Entry : g.getKnownLinks().entrySet()) {
                //todo add availability later
//                double newR = vedge.R < (1 - pedge.getRisk()) ? vedge.R / (1 - pedge.getRisk()) : vedge.R;
//                double demand = vedge.bwSigma == 0 || vedge.R == 0 ? vedge.getBW() :
//                        calculateDemand(vedge.getBW(), vedge.bwSigma, newR);
                ILink pedge = pe_Entry.getValue();
                pedge.setCost(vl.getBw() / pedge.getBw());
            }
            try {
                for (Pair<List<INode<String>>, Double> metapath : kBestNM.findKOptimalPathWithConstraints(mSrc, mDst, g,
                        vl.getBw(), vl.getLatency(), k)) {
                    vLinkCandidates.get(vl).add(metapath);
                    vl.getMpCandidates().add(new MPath(vl.getSrc(), vl.getDst(), metapath.getLeft(), metapath.getRight()));
                }
//                System.out.println("[DEBUG-MPSC] Found the following metapaths for Virtual Link src=" + vl.getSrc() + " dst=" + vl.getDst());
//                for(Pair<List<INode<String>>, Double> metapath : vLinkCandidates.get(vl))
//                    System.out.println("[DEBUG-MPSC] Cost=" + metapath.getRight() + " metapath=" + metapath.getLeft());
                clearMetaedges(g);
            } catch (Exception ex) {
                clearMetaedges(g);
                throw new RuntimeException("[MSC] No metapaths have been found for vLink:" + vl
                        + ". SC composition is not possible. Terminate...");
            }
        }
    }


    private <T> void mapMetapathResources(List<INode<T>> mpath, Map<String, INode> vNodeToPNode,
                                          Map<VirtualLink, List<INode<T>>> vLinkToPath, VirtualLink vedge) {
        //map vnodes first
        String vSrc = vedge.getSrc();
        INode pSrc = mpath.get(1);
        if (!vNodeToPNode.containsKey(vSrc))
            vNodeToPNode.put(vSrc, pSrc);
        String vDst = vedge.getDst();
        INode pDst = mpath.get(mpath.size() - 2);
        if (!vNodeToPNode.containsKey(vDst))
            vNodeToPNode.put(vDst, pDst);

        //then map vedges
        //List<ILink> path = convertPath(mpath.subList(1, mpath.size() - 1));
        if (!vLinkToPath.containsKey(vedge))//note: should be always true!
            vLinkToPath.put(vedge, mpath.subList(1, mpath.size() - 1));
    }


    private void clearMetaedges(ITopology g) {
        for (INode n : g.getKnownNodes()) {
            Set<INode> nhToRemove = new HashSet<>();
            for (Object nho : n.getNeighbors().keySet()) {
                INode nh = (INode) nho;
                if (nh.getType().equals(INode.INodeType.META_NODE))
                    nhToRemove.add(nh);
            }

            n.getNeighbors().keySet().removeAll(nhToRemove);
        }
    }

    private <T> INode<T> convertToMetanode(T vn, double capacity) {
        INode<T> mnode = new MetaNode<>(vn, capacity); //we use the special constructor for metanodes (Note: metanodes are virtual)
        for (INode pnode : vNodeCandidates.get(vn)) {
            //todo add availability later
//            double newR = vnode.R < (1 - pnode.getRisk()) ? vnode.R / (1 - pnode.getRisk()) : vnode.R;
//            double demand = vnode.targetSigma == 0 || vnode.R == 0 ? vnode.getTarget() :
//                    calculateDemand(vnode.getTarget(), vnode.targetSigma, newR);
            double metaedgeCost = capacity / pnode.getCapacity();
            ILink metaedge = new MetaLink();
            metaedge.setCost(metaedgeCost / vNodeTimes.get(vn));

            mnode.addNeighbor(pnode, metaedge);
            pnode.addNeighbor(mnode, metaedge);
        }
        return mnode;
    }


    private void constructMasterProblem(IloCplex model, IloNumVar[][] var, IloRange[][] rng, List<VirtualLink> vls,
                                        Map<ILink, Set<Pair<Integer, Integer>>> peToMPs,
                                        Map<INode, Set<Pair<Integer, Integer>>> pnToMPs) throws IloException {
        // Vars
        // First define the variables and obj values for each vnode
        IloNumExpr[] scalProds = new IloNumExpr[vls.size()]; //only 2 variable types
        for (int ve = 0; ve < vls.size(); ve++) {
            VirtualLink vedge = vls.get(ve);
            int metapathNum = vLinkCandidates.get(vedge).size();

            int[] f_lb = new int[metapathNum];
            int[] f_ub = new int[metapathNum];
            IloNumVarType[] f_t = new IloNumVarType[metapathNum];
            double[] f_objvals = new double[metapathNum];
            String[] f_names = new String[metapathNum];
            for (int i = 0; i < metapathNum; i++) {
                List<INode<String>> metapath = vLinkCandidates.get(vedge).get(i).getLeft();
                double metapathCost = vLinkCandidates.get(vedge).get(i).getRight();
                f_lb[i] = 0;
                f_ub[i] = 1;
                f_t[i] = IloNumVarType.Bool;
                f_objvals[i] = metapathCost; //track latency of metapath
                f_names[i] = "f_vs=" + vedge.getSrc()
                        + "_vd=" + vedge.getDst()
                        + "_ps=" + metapath.get(1).getID()
                        + "_pd=" + metapath.get(metapath.size() - 2).getID();
            }
            IloNumVar[] f = model.boolVarArray(metapathNum);
            for (int j = 0; j < metapathNum; j++)
                f[j].setName(f_names[j]);
            var[ve] = f;
            scalProds[ve] = model.scalProd(f, f_objvals);
        }

        // Objective Function:  maximize sum of all shadow prices
        model.addMinimize(model.sum(scalProds));

        // Constraints
        // First constraint type is an assignment constraint per each vEdge
        rng[0] = new IloRange[2];
        int vei = 0;
        for (String vn : vNodeCandidates.keySet())
            if (vNodeTimes.get(vn) == 1) //either input or output data services
            {
                VirtualLink ve = findAdjacentVLs(vn, vls).iterator().next();

                IloNumExpr[] metapathCandidates = new IloNumExpr[vLinkCandidates.get(ve).size()];
                for (int pc = 0; pc < metapathCandidates.length; pc++)
                    metapathCandidates[pc] = model.prod(1, var[vls.indexOf(ve)][pc]);
                rng[0][vei] = model.addEq(model.sum(metapathCandidates), 1); //assign one single-link chain to a single metapath
                vei++;
            }
        // Second constraint type is to ensure in an intermediate service unique assignment
        //todo Stopped HERE!!!!

        int is = 1;
        for (String vnode : vNodeCandidates.keySet())
            //consider only intermediate services (i.e., that have one incoming and one outgoing edges)
            if (vNodeTimes.get(vnode) == 2) {
                VirtualLink[] vedges = findAdjacentVLs(vnode, vls).toArray(new VirtualLink[2]);
                VirtualLink inVE = vedges[0]; //note: dirtection doesn't matter here!
                VirtualLink outVE = vedges[1];
                //stitch all metapath through equal metaedges
                Map<Pair<String, INode<String>>, List<Integer>> inMedgesToInd = mapMEsToVN(inVE, vnode);
                Map<Pair<String, INode<String>>, List<Integer>> outMedgesToInd = mapMEsToVN(outVE, vnode);
                //to track of unique metaedges
                List<IloRange> meRng = new ArrayList<>(inMedgesToInd.size() + outMedgesToInd.size());
                for (Pair<String, INode<String>> medge : inMedgesToInd.keySet()) {
                    //track all metapaths that contains medge for inVE mapping
                    List<Integer> inIndex = inMedgesToInd.get(medge);
                    IloNumExpr[] inMECandidates = new IloNumExpr[inIndex.size()];
                    for (int mpc = 0; mpc < inMECandidates.length; mpc++) //track corresponding metapath candidates
                        inMECandidates[mpc] = model.prod(1, var[vls.indexOf(inVE)][inIndex.get(mpc)]);
                    //track all metapaths that contains medge for outVE mapping
                    List<Integer> outIndex = outMedgesToInd.remove(medge); //remove edges
                    //if null incoming metaedge is unstitched -> has to be zero!
                    IloNumExpr[] outMECandidates = new IloNumExpr[outIndex == null ? 0 : outIndex.size()];
                    for (int mpc = 0; mpc < outMECandidates.length; mpc++) //track corresponding metapath candidates
                        outMECandidates[mpc] = model.prod(-1, var[vls.indexOf(outVE)][outIndex.get(mpc)]);
                    //assign a unique vnode service placement constraint
                    meRng.add(model.addEq(model.sum(model.sum(inMECandidates), model.sum(outMECandidates)), 0));
                }
                //only unstitched metaedges left here for outgoing service metaedges
                for (Pair<String, INode<String>> medge : outMedgesToInd.keySet()) {
                    List<Integer> outIndex = outMedgesToInd.get(medge);
                    IloNumExpr[] outMECandidates = new IloNumExpr[outIndex.size()];
                    for (int mpc = 0; mpc < outMECandidates.length; mpc++) //track corresponding metapath candidates
                        outMECandidates[mpc] = model.prod(-1, var[vls.indexOf(outVE)][outIndex.get(mpc)]);
                    //assign a unique vnode service placement constraint
                    meRng.add(model.addEq(model.sum(outMECandidates), 0));
                }
                rng[is] = meRng.toArray(new IloRange[meRng.size()]);
                is++;
            }

        //third constraint type is per each pnode and pedge
        //edge capacity constraints
        List<IloRange> peRng = new ArrayList<>();
        for (Map.Entry<ILink, Set<Pair<Integer, Integer>>> kMP : peToMPs.entrySet()) {
            ILink pedge = kMP.getKey();
            List<IloNumExpr> veEx = new ArrayList<>(kMP.getValue().size());
            double R = 0;
            for (Pair<Integer, Integer> k : kMP.getValue()) {
                VirtualLink vedge = vls.get(k.getKey());
                //todo add availabilty later
//                if (R < vedge.R) R = vedge.R;
//                double newR = vedge.R < (1 - pedge.getRisk()) ? vedge.R / (1 - pedge.getRisk()) : vedge.R;
//                double demand = vedge.bwSigma == 0 || vedge.R == 0 ? vedge.getBW() :
//                        calculateDemand(vedge.getBW(), vedge.bwSigma, newR);
                veEx.add(model.prod(vedge.getBw(), var[k.getKey()][k.getValue()]));
            }
            peRng.add(model.addLe(model.sum(veEx.toArray(new IloNumExpr[veEx.size()])), pedge.getBw()));
        }
        rng[vNodeCandidates.size() - 1] = peRng.toArray(new IloRange[peRng.size()]);

        //node capacity and mapping policy constraints
//        List<IloRange> pnRng1 = new ArrayList<>(peToVEs.size());
        List<IloRange> pnRng2 = new ArrayList<>();
        for (Map.Entry<INode, Set<Pair<Integer, Integer>>> kMP : pnToMPs.entrySet()) {
            INode pnode = kMP.getKey();
//                List<IloNumExpr> vnEx1 = new ArrayList<>(kMP.getValue().size());
            List<IloNumExpr> vnEx2 = new ArrayList<>(kMP.getValue().size());
            for (Pair<Integer, Integer> k : kMP.getValue()) {
                VirtualLink vedge = vls.get(k.getKey());
                List<INode<String>> mp = vLinkCandidates.get(vedge).get(k.getRight()).getKey();
                String vSrc = vedge.getSrc();
                String vDst = vedge.getDst();
                if (mp.get(1).equals(pnode)) { //pnToVNs.get(pnode).contains(vSrc) &&
//                            vnEx1.add(model.prod(vSrc.getTarget() / vSrc.getNeighbors().size(), var[k.first()][k.second()]));
                    vnEx2.add(model.prod(1.0 / vNodeTimes.get(vSrc), var[k.getLeft()][k.getRight()]));
                }
                if (mp.get(mp.size() - 2).equals(pnode)) {
//                            vnEx1.add(model.prod(vDst.getTarget() / vDst.getNeighbors().size(), var[k.first()][k.second()]));
                    vnEx2.add(model.prod(1.0 / vNodeTimes.get(vDst), var[k.getLeft()][k.getRight()]));
                }
            }
//                pnRng1.add(model.addLe(model.sum(vnEx1.toArray(new IloNumExpr[vnEx1.size()])), pnode.getTarget() - pnode.getStress()));
            pnRng2.add(model.addLe(model.sum(vnEx2.toArray(new IloNumExpr[vnEx2.size()])), 1.0));
        }
// we don't need node capacity constraints if we have policy that no more than 1 service of a chain can be placed
// on top of the physical node as metalinks assume feasible assignment
//        rng[vnr.getVNodeSize()] = pnRng1.toArray(new IloRange[pnRng1.size()]);
        rng[vNodeCandidates.size()] = pnRng2.toArray(new IloRange[pnRng2.size()]);
    }

    private Set<VirtualLink> findAdjacentVLs(String vn, Collection<VirtualLink> vls) {
        Set<VirtualLink> adjacentVLs = new HashSet<>();
        for (VirtualLink vl : vls)
            if (vl.getSrc().equals(vn) || vl.getDst().equals(vn))
                adjacentVLs.add(vl);
        return adjacentVLs;
    }

    private <T> List<ILink> convertPath(List<INode<T>> path) {
        List<ILink> pathE = new ArrayList<>(path.size() - 1);
        for (int i = 0; i < path.size() - 1; i++)
            pathE.add(path.get(i).getNeighbors().get(path.get(i + 1)));
        return pathE;
    }


    private Map<Pair<String, INode<String>>, List<Integer>> mapMEsToVN(VirtualLink vedge, String vnode) {
        Map<Pair<String, INode<String>>, List<Integer>> meToInd = new LinkedHashMap<>();
        for (int mpc = 0; mpc < vLinkCandidates.get(vedge).size(); mpc++) {
            List<INode<String>> metapath = vLinkCandidates.get(vedge).get(mpc).getLeft();
            //check it is src or dst metaedge
            INode<String> pnode = vedge.getSrc().equals(vnode) ? metapath.get(1) : metapath.get(metapath.size() - 2);
            Pair<String, INode<String>> meKey = new ImmutablePair<>(vnode, pnode);
            if (meToInd.containsKey(meKey))
                meToInd.get(meKey).add(mpc);
            else {
                List<Integer> index = new ArrayList<>();
                index.add(mpc);
                meToInd.put(meKey, index);
            }
        }
        return meToInd;
    }


    private void mapMeToPnVN(String vn, INode pn, Map<INode, Set<String>> pnToVNs) {
        if (pnToVNs.containsKey(pn))
            pnToVNs.get(pn).add(vn);
        else {
            Set<String> vnodes = new HashSet<>();
            vnodes.add(vn);
            pnToVNs.put(pn, vnodes);
        }
    }

    private void mapPnToMP(INode pn, int veInd, int mpInd, Map<INode, Set<Pair<Integer, Integer>>> pnToMPs) {
        if (pnToMPs.containsKey(pn))
            pnToMPs.get(pn).add(new ImmutablePair<>(veInd, mpInd));
        else {
            Set<Pair<Integer, Integer>> mpInds = new HashSet<>();
            mpInds.add(new ImmutablePair<>(veInd, mpInd));
            pnToMPs.put(pn, mpInds);
        }
    }

    private Map<INode, Set<Pair<Integer, Integer>>> findPnodeCapacityVars(List<VirtualLink> vls) {
        Map<INode, Set<String>> pnToVNs = new HashMap<>();
        Map<INode, Set<Pair<Integer, Integer>>> pnToMPs = new LinkedHashMap<>();
        for (VirtualLink vedge : vls)
            for (int i = 0; i < vLinkCandidates.get(vedge).size(); i++) {
                List<INode<String>> mp = vLinkCandidates.get(vedge).get(i).getKey();
                //map pnodes to vnodes and vnodes to mpaths
                mapMeToPnVN(mp.get(0).getID(), mp.get(1), pnToVNs);
                mapPnToMP(mp.get(1), vls.indexOf(vedge), i, pnToMPs);
                mapMeToPnVN(mp.get(mp.size() - 1).getID(), mp.get(mp.size() - 2), pnToVNs);
                mapPnToMP(mp.get(mp.size() - 2), vls.indexOf(vedge), i, pnToMPs);
            }
        //remove unnecessary constraints: for a single vn candidate feasibility is guaranteed by metaedges
        for (INode pnode : pnToVNs.keySet())
            if (pnToVNs.get(pnode).size() <= 1)
                pnToMPs.keySet().remove(pnode);
        return pnToMPs;
    }

    private Map<ILink, Set<Pair<Integer, Integer>>> findPedgeCapacityVars(List<VirtualLink> vls) {
        Map<ILink, Set<VirtualLink>> peToVEs = new LinkedHashMap<>();
        Map<ILink, Set<Pair<Integer, Integer>>> peToMPs = new LinkedHashMap<>();
        for (VirtualLink vedge : vls) {
            for (int i = 0; i < vLinkCandidates.get(vedge).size(); i++) {
                List<INode<String>> mp = vLinkCandidates.get(vedge).get(i).getKey();
                //map pedges to vedges and vedges to mpaths
                for (int j = 1; j < mp.size() - 1; j++) {
                    ILink pedge = mp.get(i).getNeighbors().get(mp.get(i + 1));

                    if (!peToVEs.containsKey(pedge))
                        peToVEs.put(pedge, new HashSet<>());
                    if (!peToMPs.containsKey(pedge))
                        peToMPs.put(pedge, new HashSet<>());
                    peToVEs.get(pedge).add(vedge);
                    peToMPs.get(pedge).add(new ImmutablePair<>(vls.indexOf(vedge), i));
                }
            }
        }
        //remove unnecessary constraints: for a single vn candidate feasibility is guaranteed by metaedges
        for (ILink pedge : peToVEs.keySet())
            if (peToVEs.get(pedge).size() <= 1)
                peToMPs.keySet().remove(pedge);
        return peToMPs;
    }

}