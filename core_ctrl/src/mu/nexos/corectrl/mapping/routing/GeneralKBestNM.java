package mu.nexos.corectrl.mapping.routing;


import mu.nexos.corectrl.model.ILink;
import mu.nexos.corectrl.model.INode;
import mu.nexos.corectrl.model.ITopology;
//import net.floodlightcontroller.mpsc.openflow.customservices.IRouteFinderService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;


import java.util.*;

/**
 * Created by chemo_000 on 5/24/2016.
 */
public class GeneralKBestNM {
    private PathCostComparator pathCostComparator = new PathCostComparator();

    public <T> List<Pair<List<INode<T>>, Double>> findKOptimalPathWithConstraints(INode<T> src, INode<T> dst, ITopology g, double bw, double delay, int k) {
        //prepare for gen case
        int nodesNum = g.getKnownNodes().size();
        prepareTopoForCSP(bw, g);

        if (src.getNeighbors().isEmpty() && dst.getNeighbors().isEmpty())
            throw new RuntimeException("NM can't provide path from " + src.getID() + " to " + dst.getID()
                    + " because the destination is unreachable!!!");

        return findKOptimalPathWithConstraints(src, dst, g, delay, k, nodesNum);
    }

    /* TODO: Implement method for resources with outage risks
    public <T> List<Pair<List<INode<T>>, Double>> findKOptimalPathWithConstraints(INode<T> src, INode<T> dst, ITopology g,
                                                                          double bw, double bwSigma, double R,
                                                                          double delay, int k) {
        //prepare for gen case w.r.t., risk satisfaction
        g.prepareTopoForGeneralCase(bw, bwSigma, R);

        if (src.getNeighbors().isEmpty() && dst.getNeighbors().isEmpty())
            throw new RuntimeException("NM can't provide path from " + src.getID() + " to " + dst.getID()
                    + " because the destination is unreachable!!!");

        return findKOptimalPathWithConstraints(src, dst, g, delay, k, g.getNumNodes());
    }


    public <T> List<Pair<List<INode<T>>, Double>> findKOptimalPathWithConstraints(INode<T> src, INode<T> dst, ITopology g,
                                                                                  double bw, double bwSigma, double R,
                                                                                  double delay, int k, int TTL) {
        //prepare for gen case w.r.t., risk satisfaction
        int nodesNum = routeFinder.getTopo().getKnownNodes().size();
        routeFinder.getTopo().prepareTopoForDijkstra(bw, false);

        if (src.getNeighbors().isEmpty() && dst.getNeighbors().isEmpty())
            throw new RuntimeException("NM can't provide path from " + src.getID() + " to " + dst.getID()
                    + " because the destination is unreachable!!!");

        return findKOptimalPathWithConstraints(src, dst, g, delay, k, nodesNum);
    }
    */

    private <T> List<Pair<List<INode<T>>, Double>> findKOptimalPathWithConstraints(INode<T> src, INode<T> dst, ITopology g,
                                                                                   double delay, int k, int TTL) {
        List<Map<INode<T>, List<Double>>> neighborhoods = buildNeighborhoods(g, src, dst, delay, TTL);

        return findOptimalPathWithConstraintsInt(g, src, dst, delay, Double.MAX_VALUE,
                neighborhoods, k, new ArrayList<Pair<List<INode<T>>, Double>>(k), TTL);
    }


    private <T> List<Pair<List<INode<T>>, Double>> findOptimalPathWithConstraintsInt(ITopology g, INode<T> src, INode<T> dst,
                                                                                     double delay, double cost,
                                                                                     List<Map<INode<T>, List<Double>>> neighborhoods,
                                                                                     int k, List<Pair<List<INode<T>>, Double>> paths, int TTL) {
//        System.out.println("[DEBUG-MPSC] The following neighborhhoods has been built at iter[" + k + "]:");
//        for (int i = 0; i < neighborhoods.size(); i++) {
//            System.out.println("[DEBUG-MPSC] Neighborhood[" + i + "]:");
//            for (INode n : neighborhoods.get(i).keySet())
//                System.out.println("[DEBUG-MPSC] Node id=" + n.getID() + " costs[" + neighborhoods.get(i).get(n) + "]");
//        }

        //here we assume policy that prohibits placement of two services of the same VN request to a single pnode
        Map<List<INode<T>>, List<Double>> pathCandidates = neighborhoods.size() <= 3 ? Collections.emptyMap() :
                findPathCandidates(src, dst, delay, cost, neighborhoods);

//        for (List<INode<T>> path : pathCandidates.keySet())
//            System.out.println("[DEBUG-MPSC] Found path=" + path);

        if (!pathCandidates.isEmpty()) {
            paths.addAll(convertPaths(pathCandidates, delay));
            //sort and keep best
            Collections.sort(paths, pathCostComparator);
            if (paths.size() >= k) {
                paths = paths.subList(0, k);
                cost = paths.get(paths.size() - 1).getRight(); //track the k-th path latency to bound future candidates
            }
        }
        try {
            //System.out.print(neighborhoods.size() + ".. ");
            buildNextNeighborhoods(g, neighborhoods, src, dst, delay, cost, TTL);
        } catch (Exception ex) {

            return paths; //return all known so far paths
        }
        return findOptimalPathWithConstraintsInt(g, src, dst, delay, cost, neighborhoods, k, paths, TTL);
    }

    private <T> void filterSimplePathCandidates(Map<List<INode<T>>, List<Double>> pathCandidates) {
        Set<List<INode<T>>> pathsToRemove = new HashSet<>();
        for (List<INode<T>> path : pathCandidates.keySet())
            if (new HashSet<>(path).size() != path.size())
                pathsToRemove.add(path);

        pathCandidates.keySet().removeAll(pathsToRemove);
    }

    private <T> List<Map<INode<T>, List<Double>>> buildNeighborhoods(ITopology g, INode<T> src, INode<T> dst,
                                                                     double delay, int TTL) {
        List<Map<INode<T>, List<Double>>> neighborhoods = new ArrayList<>();

        Map<INode<T>, List<Double>> currentNeighborhood = new LinkedHashMap<>();
        List<Double> initialDist = new ArrayList<>(2);
        initialDist.add(0.0); //(latency)
        initialDist.add(0.0); //(latency)
        currentNeighborhood.put(src, initialDist);
        neighborhoods.add(currentNeighborhood);
        while (!currentNeighborhood.containsKey(dst)) {
            Map<INode<T>, List<Double>> newNeighborhood = new LinkedHashMap<>();
            for (Map.Entry<INode<T>, List<Double>> entry : currentNeighborhood.entrySet()) {
                INode<T> n = entry.getKey();
                for (Map.Entry<INode<T>, ILink> neighbor : n.getNeighbors().entrySet()) {
                    if (neighbor.getKey().equals(src))
                        continue;
                    double newDelayDist = entry.getValue().get(0) + neighbor.getValue().getLatency();
                    double newCostDist = entry.getValue().get(1) + neighbor.getValue().getAvCost();
                    if (newDelayDist <= delay) // &&
                    //newCostDist <= latency) //we do not bound by latency at this stage until we get k-best paths
                    {
                        if (newNeighborhood.containsKey(neighbor.getKey())) {
                            if (newNeighborhood.get(neighbor.getKey()).get(0) > newDelayDist)
                                newNeighborhood.get(neighbor.getKey()).set(0, newDelayDist);
                            if (newNeighborhood.get(neighbor.getKey()).get(1) > newCostDist)
                                newNeighborhood.get(neighbor.getKey()).set(1, newCostDist);
                        } else {
                            List<Double> newDist = new ArrayList<>(2);
                            newDist.add(newDelayDist);
                            newDist.add(newCostDist);
                            newNeighborhood.put(neighbor.getKey(), newDist);
                        }
                    }
                }
            }

            if (newNeighborhood.isEmpty() || neighborhoods.size() >= TTL + 2) //ttl violation
                throw new RuntimeException("NM can't provide path from " + src.getID() + " to " + dst.getID()
                        + " because of constraints: delay=" + delay + ", or the destination is unreachable!!!");

            neighborhoods.add(newNeighborhood);
            currentNeighborhood = newNeighborhood;
        }
        return neighborhoods;
    }

    private <T> List<Map<INode<T>, List<Double>>> buildNextNeighborhoods(ITopology g, List<Map<INode<T>, List<Double>>> neighborhoods,
                                                                         INode<T> src, INode<T> dst, double delay, double cost, int TTL) {
        Map<INode<T>, List<Double>> currentNeighborhood = neighborhoods.get(neighborhoods.size() - 1);
        currentNeighborhood.remove(dst);
        while (!currentNeighborhood.containsKey(dst)) {
            Map<INode<T>, List<Double>> newNeighborhood = new LinkedHashMap<>();
            for (Map.Entry<INode<T>, List<Double>> entry : currentNeighborhood.entrySet()) {
                INode<T> n = entry.getKey();
                for (Map.Entry<INode<T>, ILink> neighbor : n.getNeighbors().entrySet()) {
                    if (neighbor.getKey().equals(src))
                        continue;
                    double newDelayDist = entry.getValue().get(0) + neighbor.getValue().getLatency();
                    double newCostDist = entry.getValue().get(1) + neighbor.getValue().getAvCost();
                    if (newDelayDist <= delay && newCostDist <= cost) ;
                    {
                        if (newNeighborhood.containsKey(neighbor.getKey())) {
                            if (newNeighborhood.get(neighbor.getKey()).get(0) > newDelayDist)
                                newNeighborhood.get(neighbor.getKey()).set(0, newDelayDist);
                            if (newNeighborhood.get(neighbor.getKey()).get(1) > newCostDist)
                                newNeighborhood.get(neighbor.getKey()).set(1, newCostDist);
                        } else {
                            List<Double> newDist = new ArrayList<>(2);
                            newDist.add(newDelayDist);
                            newDist.add(newCostDist);
                            newNeighborhood.put(neighbor.getKey(), newDist);
                        }
                    }
                }
            }
            if (newNeighborhood.isEmpty() || neighborhoods.size() >= TTL + 2) //ttl violation
                throw new RuntimeException("NM can't provide path from " + src.getID() + " to " + dst.getID()
                        + " because of constraints: delay=" + delay + ", or the destination is unreachable!!!");

            neighborhoods.add(newNeighborhood);
            currentNeighborhood = newNeighborhood;
        }
        return neighborhoods;
    }

    private <T> Map<List<INode<T>>, List<Double>> findPathCandidates(INode<T> src, INode<T> dst, double delay, double cost,
                                                                     List<Map<INode<T>, List<Double>>> neighborhoods) {
        Map<List<INode<T>>, List<Double>> paths = new LinkedHashMap<>();
        List<Double> distances = new ArrayList<>(2);
        distances.add(0.0);
        distances.add(0.0);
        paths.put(Collections.singletonList(dst), distances);
        int k = 2;
        Map<INode<T>, List<Double>> prevNeighborhood = neighborhoods.get(neighborhoods.size() - k);
        while (k <= neighborhoods.size()) {
            if (paths.isEmpty())
                return Collections.emptyMap();
            else {
                Map<List<INode<T>>, List<Double>> tempPaths = new LinkedHashMap<>();
                for (final Map.Entry<List<INode<T>>, List<Double>> path_entry : paths.entrySet()) {
                    INode<T> u = path_entry.getKey().get(0);
                    if (tempPaths.size() < 1000000)//TODO: this constraint is to avoid out of memory exception! Consider its removal
                        for (final Map.Entry<INode<T>, ILink> v : u.getNeighbors().entrySet()) {
                            //check if in intersection [NM main feature]
                            if (prevNeighborhood.containsKey(v.getKey()) && !path_entry.getKey().contains(v.getKey())) {
                                final double newDelayDist = path_entry.getValue().get(0) + v.getValue().getLatency();
                                final double newCostDist = path_entry.getValue().get(1) + v.getValue().getAvCost();
                                //look back during backward pass
                                double learnedDelayDist = prevNeighborhood.get(v.getKey()).get(0);
                                double learnedCostDist = prevNeighborhood.get(v.getKey()).get(1);
                                if (newDelayDist + learnedDelayDist <= delay &&
                                        newCostDist + learnedCostDist <= cost) {
                                    //create new path candidate
                                    List<INode<T>> newPath = new ArrayList<INode<T>>() {
                                        {
                                            add(v.getKey());
                                            addAll(path_entry.getKey());
                                        }
                                    };
                                    List<Double> newDistances = new ArrayList<Double>() {
                                        {
                                            add(newDelayDist);
                                            add(newCostDist);
                                        }
                                    };
                                    tempPaths.put(newPath, newDistances);
                                }
                            }
                        }
                }
                // set up for next hop cycle
                paths = tempPaths;
                ++k;
                if (k <= neighborhoods.size())
                    prevNeighborhood = neighborhoods.get(neighborhoods.size() - k);
            }
        }
        return paths;
    }

    private <T> List<Pair<List<INode<T>>, Double>> convertPaths(Map<List<INode<T>>, List<Double>> pathCandidates, double delay) {
        List<Pair<List<INode<T>>, Double>> constrainedPaths = new ArrayList<>();

        for (Map.Entry<List<INode<T>>, List<Double>> entry_path : pathCandidates.entrySet())
            if (entry_path.getValue().get(0) <= delay)
                constrainedPaths.add(new ImmutablePair<>(entry_path.getKey(), entry_path.getValue().get(1)));

        return constrainedPaths;
    }

    public void prepareTopoForCSP(double bw, ITopology g) {
        for (INode n : g.getKnownNodes()) {
            n.setMinDist(Double.POSITIVE_INFINITY);
            n.setPredecessor(null);
            n.clearAllPredecessorsAndMinDists();
            n.clearDominantPaths();
        }

        for (ILink l : g.getKnownLinks().values())
            if (l.getBw() < bw)
                l.setAvCost(Double.POSITIVE_INFINITY);
            else
                l.setAvCost(l.getCost());
    }
}

/**
 * Path Cost comparator sorts in ascending order path costs
 */
class PathCostComparator implements Comparator {

    public int compare(Object o1, Object o2) {

        //estimate first metapath latency
        Pair<List<INode>, Double> path1 = (Pair<List<INode>, Double>) o1;
        Pair<List<INode>, Double> path2 = (Pair<List<INode>, Double>) o2;
        double cost1 = path1.getRight();
        double cost2 = path2.getRight();

        //compare both costs
        if (cost1 < cost2) return -1;
        if (cost1 == cost2) return 0;
        if (cost1 > cost2) return 1;

        return 1;  //never gets here but javac complains
    }

}
