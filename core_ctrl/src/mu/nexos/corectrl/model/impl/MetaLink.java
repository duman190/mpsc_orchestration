package mu.nexos.corectrl.model.impl;

import mu.nexos.corectrl.model.INode;


/**
 * Created by dmitriichemodanov on 2/28/18.
 */
public class MetaLink extends LinkBaseAbstract {

    public MetaLink()
    {
        super();
    }

    @Override
    public double getBw() {
        return Double.MAX_VALUE;
    }

    @Override
    public double getCapacity() {
        return Double.MAX_VALUE;
    }

    @Override
    public double getLatency() {
        return 0.0;
    }
}
