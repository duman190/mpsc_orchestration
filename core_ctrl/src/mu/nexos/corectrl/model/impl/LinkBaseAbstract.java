package mu.nexos.corectrl.model.impl;

import mu.nexos.corectrl.model.ILink;
//import net.floodlightcontroller.mpsc.utils.AllocatedBwEstimator;
//import org.projectfloodlight.openflow.types.DatapathId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by chemo_000 on 3/10/2015.
 */
public abstract class LinkBaseAbstract implements ILink
{
    protected static Logger log = LoggerFactory.getLogger(LinkBaseAbstract.class);

    private double cost = 0;

    private double avCost = 0; //used by routing algorithms to allow link pruning by setting up an infinite cost of its traversing

    public LinkBaseAbstract()
    {
    }

    /*
    protected double calculateAllocatedBwForPort(DatapathId dpid, short port)
    {
        return new AllocatedBwEstimator().estimateAllocatedBw(dpid, port, topology.getFlowPusher());
    }
*/

    public double getCost()
    {
        return cost;
    }

    public void setCost(double cost)
    {
        this.cost = cost;
        this.avCost = cost;
    }

    public double getAvCost()
    {
        return avCost;
    }

    public void setAvCost(double cost)
    {
        this.avCost = cost;
    }
}
