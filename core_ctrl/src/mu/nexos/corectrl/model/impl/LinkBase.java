package mu.nexos.corectrl.model.impl;

import mu.nexos.corectrl.model.INode;

/**
 * Created by dmitriichemodanov on 9/17/18.
 */
public class LinkBase extends LinkBaseAbstract {

    private double bw;
    private double capacity;
    private double latency;

    public LinkBase (double bw, double capacity, double latency)
    {
        super();

        this.bw = bw;
        this.capacity = capacity;
        this.latency = latency;
    }

    @Override
    public double getBw() {
        return this.bw;
    }

    @Override
    public double getCapacity() {
        return this.capacity;
    }

    @Override
    public double getLatency() {
        return this.latency;
    }
}
