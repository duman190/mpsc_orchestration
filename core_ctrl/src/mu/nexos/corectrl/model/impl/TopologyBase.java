package mu.nexos.corectrl.model.impl;


import mu.nexos.corectrl.model.ILink;
import mu.nexos.corectrl.model.INode;
import mu.nexos.corectrl.model.ITopology;
import mu.nexos.corectrl.distrctrl.DistrCtrlHolder;
import mu.nexos.corectrl.distrctrl.DistrCtrlKey;
import mu.nexos.corectrl.web.SwitchClientResource;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by chemo_000 on 2/10/2015.
 */
public class TopologyBase implements ITopology {
    protected static Logger log = LoggerFactory.getLogger(ITopology.class);
    private Map<DistrCtrlKey, Set<INode>> knownNodes;
    private Map<Object, INode> idToNode;
    private Map<Pair<INode, INode>, ILink> knownLinks;

    public TopologyBase() {
        this.knownNodes = new HashMap<>();
        this.idToNode = new HashMap<>();
        this.knownLinks = new HashMap<>();
    }

    public void updateTopoCache() {
        System.out.println("[DEBUG] The client tries to update the topo cache");
        //flush all cache
        knownNodes.clear();
        idToNode.clear();
        knownLinks.clear();

//        if(GeneralConfigParser.getInstance().DYNAMIC_GRAPH)
//            TopoConfigParser.Update();

        //recursive call distributed SDN cntrls to update topo state
        for (DistrCtrlKey key : DistrCtrlHolder.instance.getSubscribers()) {
            int nodeSize = idToNode.size();
            System.out.println("[DEBUG] The client tries to retrieve the topo of the switch IP=" + key.getIP() + ", port=" + key.getPort());
            Set<INode> nodes = new HashSet<>();
            Map<Pair<INode, INode>, ILink> links = new HashMap<>();
            SwitchClientResource.retrieveSwitchTopo(key, nodes, links);
            knownNodes.put(key, nodes);
            for (INode n : nodes)
                if (!idToNode.containsKey(n.getID()))
                    idToNode.put(n.getID(), n);
            for (Pair<INode, INode> linkKey : links.keySet()) {
                INode src = idToNode.get(linkKey.getLeft().getID());
                INode dst = idToNode.get(linkKey.getRight().getID());
                Pair<INode, INode> newlinkKey = new ImmutablePair<>(src, dst);
                Pair<INode, INode> newlinkKey1 = new ImmutablePair<>(dst, src);
                if (!knownLinks.containsKey(newlinkKey) && !knownLinks.containsKey(newlinkKey1))
                    knownLinks.put(newlinkKey, links.get(linkKey));
            }
            System.out.println("[DEBUG] The topo has been successfully retrieved with " + (idToNode.size() - nodeSize) + " new node(s)");
        }

        if (log.isInfoEnabled())
            log.info("[MPSCCoreCtrl-Topology] Topology Cache has been successfully updated!");

        //init neighbors based on cache!
        initNeighbors();
    }

    private void initNeighbors() {
        for (INode n : idToNode.values())
            n.clearNeighbors();

        for (Map.Entry<Pair<INode, INode>, ILink> entry : knownLinks.entrySet()) {
            entry.getKey().getLeft().addNeighbor(entry.getKey().getRight(), entry.getValue());

//            System.out.println("[DEBUG-MPSC] Node id=" + entry.getKey().getLeft().getID() + " hash=" + entry.getKey().getLeft().hashCode()
//                    + " is a neighbor of node id=" + entry.getKey().getRight().getID() + " hash=" + entry.getKey().getRight().hashCode());


            entry.getKey().getRight().addNeighbor(entry.getKey().getLeft(), entry.getValue());

//            System.out.println("[DEBUG-MPSC] And node id=" + entry.getKey().getRight().getID() + " hash=" + entry.getKey().getRight().hashCode()
//                    + " is a neighbor of node id=" + entry.getKey().getLeft().getID() + " hash=" + entry.getKey().getLeft().hashCode() + ", respectively.");


//            System.out.println("[DEBUG-MPSC] Node id=" + entry.getKey().getLeft().getID() + " neighbors=" + entry.getKey().getLeft().getNeighbors());
//            System.out.println("[DEBUG-MPSC] Node id=" + entry.getKey().getRight().getID() + " neighbors=" + entry.getKey().getRight().getNeighbors());
        }

    }

    @Override
    public void prepareTopoForDijkstra(double bw, boolean isSPF) {
        for (INode n : idToNode.values()) {
            n.setMinDist(Double.POSITIVE_INFINITY);
            n.setPredecessor(null);
            n.clearAllPredecessorsAndMinDists();
            n.clearDominantPaths();
        }

        for (ILink l : knownLinks.values())
            if (l.getBw() < bw)
                l.setAvCost(Double.POSITIVE_INFINITY);
            else if (isSPF)
                l.setAvCost(1);
            else
                l.setAvCost(l.getCost());
        /*
        //prepare nodes
        for (Node n : getNodesArray()) {
            n.nm_minDist.clear();
            n.nm_predecessor.clear();
            n.minDistance = Double.POSITIVE_INFINITY;
            n.previous = null;
            n.dstAheadVector[0] = Double.POSITIVE_INFINITY;
            n.dstAheadVector[1] = Double.POSITIVE_INFINITY;
            n.clearDominantPaths();
        }
        */
    }

    @Override
    public Set<INode> getSwitchDependentNodes(DistrCtrlKey key) {

        return this.knownNodes.get(key);
    }

    @Override
    public Collection<INode> getKnownNodes() {
        return idToNode.values();
    }

    @Override
    public <T> INode<T> getNodeWithID(T id) {
        if (idToNode.containsKey(id.toString()))
            return idToNode.get(id.toString());

        throw new UnsupportedOperationException("[NM-model]" + id.getClass() +
                " type of node ID currently isn't supported or "
                + id + " node is unknown for controller!");
    }

    public Map<Pair<INode, INode>, ILink> getKnownLinks() {
        return this.knownLinks;
    }
}
