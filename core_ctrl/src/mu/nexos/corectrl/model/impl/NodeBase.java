package mu.nexos.corectrl.model.impl;

import mu.nexos.corectrl.model.ILink;
import mu.nexos.corectrl.model.INode;

import java.util.Map;

/**
 * Created by dmitriichemodanov on 9/17/18.
 */
public class NodeBase<T> extends NodeBaseAbstract {
    private double capacity;
    private INodeType type;
    private double stress;

    public NodeBase(T id, INodeType type, double capacity, double stress)
    {
        super(id);

        this.capacity = capacity;
        this.type = type;
        this.stress = stress;
    }

    @Override
    public double getCapacity() {
        return capacity;
    }

    @Override
    public double getStress() {
        return stress;
    }

    @Override
    public void setStress(double stress) {
        this.stress = stress;
    }

    @Override
    public INodeType getType() {
        return this.type;
    }
}
