package mu.nexos.corectrl.model;

import mu.nexos.corectrl.model.impl.TopologyBase;

/**
 * Created by dmitriichemodanov on 9/14/18.
 */
public class TopoHolder {
    public static TopoHolder instance = new TopoHolder();

    private ITopology topo;

    private TopoHolder()
    {
        topo = new TopologyBase();
    }

    public ITopology getTopo()
    {
        return this.topo;
    }

    public void updateTopoCache()
    {
        this.topo.updateTopoCache();
    }
}
