package mu.nexos.corectrl.model;


import mu.nexos.corectrl.MPSCConstants;

/**
 * Created by chemo_000 on 2/6/2015.
 */
public interface ILink extends MPSCConstants
{
    public double getBw();

    public double getCapacity();

    public double getCost();

    public void setCost(double cost);

    public double getLatency();

    public double getAvCost();

    public void setAvCost(double cost);
}
