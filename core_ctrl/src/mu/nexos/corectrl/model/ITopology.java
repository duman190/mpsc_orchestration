package mu.nexos.corectrl.model;

import mu.nexos.corectrl.distrctrl.DistrCtrlKey;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Created by chemo_000 on 2/6/2015.
 */
public interface ITopology
{
    <T> INode<T> getNodeWithID(T id);

    void updateTopoCache();

    void prepareTopoForDijkstra(double bw, boolean isSPF);

    Set<INode> getSwitchDependentNodes (DistrCtrlKey key);

    Collection<INode> getKnownNodes();

    Map<Pair<INode,INode>, ILink> getKnownLinks();
}
