<?php
  $require_login = false;
  require_once("./classes/NaaS.php");
  
  if(isset($_POST['username'])) {
    $_SESSION['username'] = $_POST['username'];
    header("Location: ./index.php");
  }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>NaaS Project</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="css/app.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        
    </head>
    <body>
    
      <div class="container">
        <h1>Login</h1>
        <form method="post">
          <div class="form-group">
            <label>Username:</label>
            <input type="text" name="username" class="form-control" />
          </div>
          <div class="form-group">
            <label>Password:</label>
            <input type="password" name="password" class="form-control" />
          </div>
          <div class="form-group">
            <input type="submit" value="Login" class="btn btn-primary" />
          </div>
        </form>
      </div>

    </body>
</html>