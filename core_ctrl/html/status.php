<?php
    $tab = "status";
    require_once("./includes/header.php");
?>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h3>Status of Links</h3>
                </div>
                <div class="col-md-6">
                    <span class="btn btn-default btn-sm pull-right" style="margin-top: 20px;"><span class="glyphicon glyphicon-refresh"></span> Refresh</span>
                </div>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Src Service Name</th>
                        <th>Src Service IP</th>
                        <th>Dst Service Name</th>
                        <th>Dst Service IP</th>
                        <th>Allocated Bandwidth (Mbps)</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="warning">
                        <td>a</td>
                        <td>124.25.68.91</td>
                        <td>b</td>
                        <td>234.21.54.108</td>
                        <td>5</td>
                        <td>Pending</td>
                    </tr>
                    <tr class="success">
                        <td>b</td>
                        <td>234.21.54.108</td>
                        <td>c</td>
                        <td>23.49.121.9</td>
                        <td>10</td>
                        <td>Allocated</td>
                    </tr>
                </tbody>
            </table>
            <p>
                Total Allocated Bandwidth: 10/15 Mbps
            </p>
        </div>

<?php require_once("./includes/footer.php"); ?>