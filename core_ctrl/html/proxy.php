<?php
  $require_login = true;
  require_once("./classes/NaaS.php");

  $request = array('type' => 'none');
  $output = "";
  $invalid_request = false;
  

  if(isset($_GET['request'])) { // GET request
    $request['type'] = 'GET';
    $request['name'] = $_GET['request'];
  }
  else if(isset($_POST)) { // POST request
    $request['type'] = 'POST';
    $request['name'] = $_POST['request'];
    $request['data']['source'] = $_POST['source'];
    $request['data']['srcIP'] = "-";
    $request['data']['src_demand'] = doubleval($_POST['src_demand']);
    $request['data']['destination'] = $_POST['destination'];
    $request['data']['dstIP'] = "-";
    $request['data']['dst_demand'] = doubleval($_POST['dst_demand']);
    $request['data']['bandwidth'] = doubleval($_POST['bandwidth']);
	$request['data']['latency'] = doubleval($_POST['latency']);
    $request['data']['status'] = "PENDING";
  }
  
  $request_whitelist = array( // Valid POST and GET request options
    'GET' => array( // Maps proxy request name to REST request name
      'user_virtual_links' => 'SC/',
	  'remove_all_vls' => 'Remove_all_SCs/',
      'admin_physical_links' => 'PN/'
    ),
    'POST' => array( // Maps proxy request name to REST request name
      'user_request_link' => 'SC/'
    )
  );
  
  if(isset($request_whitelist[$request['type']][$request['name']])) {
    $requestStr = $control->get_controller_url() . $request_whitelist[$request['type']][$request['name']] . $control->get_username();
  
    switch($request['type']) { // Decide type of request and further actions
      case 'GET': // GET request
          //echo $requestStr;
          $ch = curl_init($requestStr); // Live cURL
          $timeout = 5;
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
          $output = curl_exec($ch);
          curl_close($ch);
        break;
      
      case 'POST': // POST request
          $timeout = 5;
          $data_string = "[" . json_encode($request['data']) . "]";
          $ch = curl_init($requestStr); // Live cURL
          
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
          curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);  
          curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
              'Content-Type: application/json',                                                                                
              'Content-Length: ' . strlen($data_string))                                                                       
          );   
          $output = curl_exec($ch);
          curl_close($ch);
        break;
        
      default: // Not a GET or POST request
        $invalid_request = true;
    }
  }
  else {
    $invalid_request = true;
  }
  
  if($invalid_request) { // Error message
    $output = "{'error': 'Invalid Request' , 'type': \"" . $request['type'] . "\"}";    
  }
  
  // Print out json result
  header("Content-Type: application/json");
  echo $output;

?>