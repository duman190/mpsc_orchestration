Metapath Composite Variable-based Reliable Service Chain Orchestration utilizing Hierarchical Control Plane
=================
In this project, we develop a novel Reliable Service Chain Orchestration framework that utilizes novel Metapath Composite Variable approach (MRSCO) to achieve a near optimal (99%) service chain composition over practically sized problems (e.g., of US Tier-1 (~300 nodes) and regional (~600 nodes) infrastructures) in seconds. Moreover, to avoid a single point of failure or congestion, we orchestrate service chains using a hierarchical control plane, i.e., both centralized and distributed control planes. Our software stack utilizes Open Daylight Floodlight controller as the main Software-Defined Networking (SDN) system for provisioning networking resources to support function connectivity demands as well as provides bare metal machine IP address for service function placement and execution (i.e., either direct or via use of e.g., Docker containers).

Authors
=================
2019 VIMAN laboratory, Computer Science Department, University of Missouri-Columbia.

```
Updated Mar 29, 2019 by Dmitrii Chemodanov
```

All feedback appreciated to Prof. Prasad Calyam (email: calyamp@missouri.edu)

License
=================
This project is licensed under the GNU General Public License - see the [LICENSE.md](LICENSE.md) file for details


How does MRSCO work?
==================
Once logged to a web portal, user specify its application/processing pipiline (compute/netowrk) demands in a form of a service chain request, e.g., in the case of the common object tracking pipiline it can a pre-processing <-> object tracking <-> analysis service function chain.
MRSCO then calls distributed controller from the centralized control plane to retrieve the latest physical topology with the available resources. After that, MRSCO solves NP-hard integer multi-commodity-chain flow problem using metapath composite variable approach to (near) optimally compose provided service chain request with the physical resources as shown in the figure below:

![Figure](/img/general.png)

**Figure 1**: *Illustrative example of the geo-distributed latency-sensitive service function chain used for the real-time object tracking pipeline.*

Finally, MRSCO sends allocate requests (for compute/network resource provisioning) to distributed controllers and reports results back (including corresponding function bare metal IP addresses) to the portal.  After that, user is welcome to login to the corresponding physical resorces and lunch its code to process corresponding data within data plane as shown in figure below:

![Figure](/img/architecture.png)

**Figure 2**: *System architecture of our reliable service chain orchestration prototype includes the following main logical components: (i) control applications are responsible
for service chain orchestration; (ii) the simple coordination layer (SCL) and root controllers are responsible for guaranteeing consistency in the distributed control plane (will be available with future software releases); (iii) SDN is responsible for traffic steering in data plane; and (iv) Hypervisor is responsible for provisioning hardware resources to process service functions.*

What is inside?
================
The source code for both MRSCO centralized and distributed control planes, which are currently implemented as modules of the Floodlight OpenFlow controller (http://www.projectfloodlight.org/floodlight/) and allocate user service chain requests over physical OpenFlow-enabled network providing bare metal machine IP addresses to end-users for their funciton placement and execution. 

Distribution
================
The distribution tree contains: 

* README

	- this file
    
* LICENSE

	- GNU license file

* img/ (folder with images used in README)
    
* core_ctrl/ (centralized control plane implementation)	

    - html/ (web portal) 
    
    - build.xml (build file for ant)    
    
    - lib/      (java library dependencies)
    
    - src/      (java source files)
    
* distr_ctrl/ (distributed control plane implementation)	

    - build.xml (build file for ant)    
    
    - lib/      (java library dependencies)
    
    - src/      (java source files)    
    
* experiments/ (supplimentary files such as GENI RSpecs for early experiments)
        
    
Distributed Control Plane Configuration
============
topo-config.xml -- used for MPSC SDN and Hypervisor modules to specify physical link properties (e.g., capacity or an arbitrary cost) and server properties (e.g., number of CPU cores).

mpsc-config.xml -- general MPSC config to specify basic properties (e.g., OF flows timeout, dynamic graph discovery, turn on/off QoS mode, etc.).

Compilation and run
============
Compiling and run of this software requires Apache, Ant, Java 1.8, IBM ILOG CPLEX Optimizer (for centralized control plane only) and Docker hypervisor (optional; for data plane hosts only) installed. These can be downloaded respectively from:

http://ant.apache.org/bindownload.cgi

http://jakarta.apache.org/ant/index.html 

http://java.sun.com/j2se/

http://www.ibm.com/products/ilog-cplex-optimization-studio

https://www.docker.com/

## Compile and Run (Stage 1: Centralized Control Plane)
* Specify path to cplex in build.xml file

    ```
    <property name="cplex_path" location="/Users/user1/Applications/IBM/ILOG/CPLEX_Studio127/cplex/bin/x86-64_osx"/> 
    ```

* Clean

    ```
    ant clean 
    ```
    
* Compile

    ```
    ant
    ```
    
* Run

    ```
    ant run
    ```

* Copy *html/* to root directory of the apache server and launch the portal via browser of your choice
    

## Compile and Run (Stage 2: Distributed Control Plane - repeat for each distributed controller)

**Note: for correct work use OF v1.0, e.g., see for OVS - http://docs.openvswitch.org/en/latest/faq/openflow/**

* Specify specify IP address of the central controller (launched upon stage 1 completion) in the general config file

* Provide corresponding (to particular controller) physical resources capacity information via topo.xml config file

* Clean

    ```
    ant clean 
    ```
    
* Compile

    ```
    export ANT_OPTS=-Xmx512m;
    ant
    ```
    
* Create OF Storage    
    ```
    sudo mkdir /var/lib/floodlight;
    sudo chmod 777 /var/lib/floodlight
    ```

* Run

    ```
    ant run
    ```


Object Tracking Case Study Experiment Results
==================
Here we show how our MRSCO can be used to improve data throughput and tracking time for the case study object tracking application shown in Figure 1 using geo-distributed latency-sensitive service chain processing w.r.t. the common core cloud computing. To this aim, we choose the VGA resolution data to track pedestrians in a crowd [Ellis, PETS 2009]. The pre-processing step is performed for every image that needs to undergo adaptive contrast enhancement with Imagemagick tool before being used for tracking in the core cloud. The adaptive contrast enhancement requires global image information and thus needs to read in image data into memory, and operates on every pixel. All images are pyramidal tiled TIFF (Tagged Image File Format) and the pre-processing retains the tile geometry.

![Figure](/img/testbed.png)

**Figure 3**: *Data flows in the allocated in GENI edge/core cloud testbed: (a) object tracking data flow interferes with concurrent flow on the s2-s1 link as regular network sends data through the best (the shortest) path; (b) by using our reliable service chain orchestration prototype, we chain tracking services with QoS guarantees which avoids congestion by redirection of concurrent flow through longer path s2-s3-s1. Furthermore, by allocating image pre-processing service function at the edge cloud (to h2 instead h1) it enables
near real-time tracking.*

Our testbed setup includes 6 virtual machines (VMs) in the GENI platform as shown in Figure 3, where three of these VMs emulate OpenFlow switches (s1, s2 and s3) and others are regular hosts (h1, h2 and h3). We assume a 4G-LTE network configuration at the edge. Hence, each host-to-switch link has 100 Mbps bandwidth without delay, each switch-to-switch link has only 40 or 50 Mbps bandwidth and 5 ms transmission delay to emulate congested and damaged network infrastructure (e.g., in a disaster scenario). Using our reliable SFC orchestration prototype, the tracking service function is allocated on h1 (quad-core CPU and 4GB of RAM) that acts at a core cloud site. At the same time, the pre-processing service function is allocated on h2
(double-core CPU and 2GB of RAM) that acts at a edge cloud site. Finally, h3 (single-core CPU and 1GB of RAM) consumes raw data from h2 by acting as a remote storage at core cloud site. The h3 is configured with cross-traffic flows consumption such that it interferes with our object tracking service chain traffic. We call this cross-traffic as the ‘concurrent flow’, and the object tracking service chain traffic as the ‘main flow’.

**Table I**: *Object tracking case study results.*

![Figure](/img/table.png)

Table I shows the final timing results computed with estimate 95% confidence intervals for service chain and cloud computing cases. For each trial, we used a 300 frame video sequence and measured several application performance metrics such as estimated throughout, tracking time, waiting time and total time. In our settings, we are able to pre-process frames faster in the core cloud computing scenario than when a geo-distributed latency-sensitive service chain is used. However, due to congestion in best-effort IP network and the absence of raw data at the core cloud site, we cannot track frames in real-time (i.e., with 0 waiting time) in the core cloud computing scenario. Whereas by orchestrating a geo-distributed latency-sensitive service chain of our object tracking pipeline, we can track frames in near real time at 3-4 Hz.

## Steps to Reproduce Experiment
* Use the following RSpec file to provision resources in GENI (Note: for Object Tracking to work properly, use Wisconsin IstaGENI aggregation manager for the core cloud site)

    ```
    experiments/vcc_cloud_fog.rspec 
    ```
    
* Start sending concurrent traffic from h2 to h3

* Start sending main traffic (imagery) from h2 to h1

* (Service Chain case only:) While performing data processing via the allocated SFC, start pre-processing concurrently with the previous step

* Wait until at least the first frame has been transferred

* (Cloud Computing case only:) Start pre-processing before the next step (in this case the tracking service function has to wait for each frame when its preprocessing ends)

* Start tracking

* Wait until all main traffic has been transferred

* Terminate both the service functions and data transfers